unit uFrmForces;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  ExtCtrls, uMGSettings, uDM, uMGBasicTypes;

type

  { TFrmForces }

  TFrmForces = class(TForm)
    imgForces: TImage;
    MainMenu: TMainMenu;
    miControlField: TMenuItem;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FContactPoints: T3DContactPointArray;
    FScaleLeft: Double;
    FScaleTop: Double;
    FScaleWidth: Double;
    FScaleHeight: Double;
    procedure SetContactPoints(AValue: T3DContactPointArray);
    procedure SetScaleLeft(AValue: Double);
    procedure SetScaleTop(AValue: Double);
    procedure SetScaleWidth(AValue: Double);
    procedure SetScaleHeight(AValue: Double);
    { private declarations }
  public
    { public declarations }
    procedure DrawDirection(Vector: TXY; MaxX: Double = 0; MaxY: Double = 0;
      LineColor: TColor = clBlack; First: Boolean = False);
    procedure ScaleLine(P1, P2: TXY);
    procedure ScalePoint(P: TXY);
    procedure ScaleCircle(Center: TXY; Radius: Double);
    procedure Clear;
    function ScalePointToClient(Point: TXY): TPoint;
    function ScaleSizeToClient(Size: TXY): TPoint;
    property ContactPoints: T3DContactPointArray read FContactPoints
      write SetContactPoints;
    property ScaleLeft: Double read FScaleLeft write SetScaleLeft;
    property ScaleTop: Double read FScaleTop write SetScaleTop;
    property ScaleWidth: Double read FScaleWidth write SetScaleWidth;
    property ScaleHeight: Double read FScaleHeight write SetScaleHeight;
  end;

const
  txtControlField = '<control field>';

var
  FrmForces: TFrmForces;

implementation

{$R *.lfm}

{ TFrmForces }

procedure TFrmForces.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if DM.Settings.ShowForces then
    DM.Settings.ShowForces:=False;
end;

procedure TFrmForces.FormCreate(Sender: TObject);
begin
  Self.ClientWidth:=Self.ClientHeight;
  imgForces.Canvas.FillRect(imgForces.ClientRect);
end;

procedure TFrmForces.SetContactPoints(AValue: T3DContactPointArray);
var
  I: Integer;
  ContactPoint: TXYLngZVar;
  DrawingPoint: TPoint;
  ZoomedPixelWidth, ZoomedPixelHeight: Integer;
begin
  if FContactPoints=AValue then Exit;
  FContactPoints:=AValue;
  // Fifteen zoomed pixels -> width of one is 1/15 of the form width:
  ZoomedPixelWidth:=round(imgForces.ClientWidth / 15);
  // The same with height:
  ZoomedPixelHeight:=round(imgForces.ClientHeight / 15);
  // We want to draw 1 zoomed pixel wide dots (but cannot use pixel height):
  imgForces.Canvas.Pen.Width:=ZoomedPixelWidth;
  // For each contact point:
  for I:=0 to High(ContactPoints) do
  begin
    ContactPoint:=FContactPoints[I];
    // If we catch a zero contact point, let's end the processing
    if (ContactPoint.X = 0) and (ContactPoint.Y = 0) then
      Break;
    // Let's calculate the drawing point of the contact point:
    DrawingPoint:=Point(
        round((ContactPoint.X + 0.5) * ZoomedPixelWidth),
        round((ContactPoint.Y + 0.5) * ZoomedPixelHeight));
    // Set color for the point:
    imgForces.Canvas.Pen.Color := clYellow;
    // Draw the contact point:
    imgForces.Canvas.Line(DrawingPoint, DrawingPoint);
  end;
end;

procedure TFrmForces.SetScaleHeight(AValue: Double);
begin
  if FScaleHeight=AValue then Exit;
  FScaleHeight:=AValue;
end;

procedure TFrmForces.SetScaleLeft(AValue: Double);
begin
  if FScaleLeft=AValue then Exit;
  FScaleLeft:=AValue;
end;

procedure TFrmForces.SetScaleTop(AValue: Double);
begin
  if FScaleTop=AValue then Exit;
  FScaleTop:=AValue;
end;

procedure TFrmForces.SetScaleWidth(AValue: Double);
begin
  if FScaleWidth=AValue then Exit;
  FScaleWidth:=AValue;
end;

procedure TFrmForces.DrawDirection(Vector: TXY; MaxX: Double; MaxY: Double;
  LineColor: TColor; First: Boolean);
var
  Msg: String;
  procedure AppendToMsg(TextToAppend: String);
  begin
    if Msg > '' then Msg += ' and ';
    Msg += TextToAppend;
  end;
begin
  if First then imgForces.Canvas.Clear;
  if MaxX > 0 then
  begin
    ScaleWidth := trunc(MaxX * 2);
    ScaleLeft := -MaxX;
  end;
  if MaxY > 0 then
  begin
    ScaleHeight := trunc(MaxY * 2);
    ScaleTop := -MaxY;
  end;

  // Check for extreme values
  if Vector.X >= MaxInt then
  begin
    Vector.X := MaxInt - 1;
    AppendToMsg('X = a rocket!');;
  end;
  if Vector.Y >= MaxInt then
  begin
    Vector.Y := MaxInt - 1;
    AppendToMsg('Y = a rocket!');
  end;
  if Vector.X <= -MaxInt + 1 then
  begin
    Vector.X := -MaxInt + 2;
    AppendToMsg('X = a meteor!');;
  end;
  if Vector.Y <= -MaxInt + 1 then
  begin
    Vector.Y := -MaxInt + 2;
    AppendToMsg('Y = a meteor!');;
  end;
  if Msg > '' then miControlField.Caption := Msg;

  // Draw the line
  imgForces.Canvas.Pen.Color := LineColor;
  imgForces.Canvas.Pen.Width := 1;
  ScaleLine(CXY(0, 0), Vector);
end;

procedure TFrmForces.ScaleLine(P1, P2: TXY);
var
  ClientP1, ClientP2: TPoint;
begin
  ClientP1 := ScalePointToClient(P1);
  ClientP2 := ScalePointToClient(P2);
  imgForces.Canvas.Line(ClientP1, ClientP2);
end;

procedure TFrmForces.ScalePoint(P: TXY);
begin
  ScaleLine(P, P);
end;

procedure TFrmForces.ScaleCircle(Center: TXY; Radius: Double);
var
  ClientCenter: TPoint;
  ClientRadius: TPoint;
begin
  ClientCenter := ScalePointToClient(Center);
  ClientRadius := ScaleSizeToClient(CXY(Radius, Radius));
  imgForces.Canvas.Ellipse(
    round(ClientCenter.X - ClientRadius.X),
    round(ClientCenter.Y - ClientRadius.Y),
    round(ClientCenter.X + ClientRadius.X),
    round(ClientCenter.Y + ClientRadius.Y)
  );
end;

procedure TFrmForces.Clear;
begin
  imgForces.Canvas.Clear;
end;

function TFrmForces.ScalePointToClient(Point: TXY): TPoint;
begin
  Result.X := round(imgForces.ClientWidth / ScaleWidth *
    (Point.X - ScaleLeft));
  Result.Y := round(imgForces.ClientHeight / ScaleHeight *
    (Point.Y - ScaleTop));
end;

function TFrmForces.ScaleSizeToClient(Size: TXY): TPoint;
begin
  Result.X := round(imgForces.ClientWidth / ScaleWidth * Size.X);
  Result.Y := round(imgForces.ClientHeight / ScaleHeight * Size.Y);
end;

end.

