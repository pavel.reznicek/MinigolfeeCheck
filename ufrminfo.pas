unit uFrmInfo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, uMGSharedItems, uMGUtils;

type

  { TFrmInfo }

  TFrmInfo = class(TForm)
    btOK: TButton;
    imgLogo: TImage;
    lblMinigolfeeCheck: TLabel;
    memoInfo: TMemo;
    panTitle: TPanel;
    procedure btOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FrmInfo: TFrmInfo;

implementation

{$R *.lfm}

{ TFrmInfo }

procedure TFrmInfo.FormCreate(Sender: TObject);
var
  ReadMeFilePath: String;
begin
  Icon := Application.Icon;
  //lblMinigolfeeCheck.Caption := lblMinigolfeeCheck.Caption + ' ' + Version;
  lblMinigolfeeCheck.Caption := ApplicationName + ' ' + GetMGVersion;
  ReadMeFilePath := OurPath('README.md');
  memoInfo.Lines.LoadFromFile(ReadMeFilePath);
end;

procedure TFrmInfo.btOKClick(Sender: TObject);
begin
  Close;
end;

end.

