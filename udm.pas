unit uDM;
{ A data module to initiate the settings before any form is created }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, uMGSettings;

type

  { TDM }

  TDM = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    Settings: TMGSettings;
  end;

var
  DM: TDM;

implementation

{$R *.lfm}

{ TDM }

procedure TDM.DataModuleCreate(Sender: TObject);
begin
  Settings:=TMGSettings.Create;
end;

procedure TDM.DataModuleDestroy(Sender: TObject);
begin
  Settings.Free;
end;

end.

