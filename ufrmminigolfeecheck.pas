unit uFrmMinigolfeeCheck;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  ExtCtrls, StdCtrls, Menus, LCLType, Math, LazFileUtils, LazUTF8, uplaysound,
  uMGSharedItems, uMGBasicTypes, uMGSettings, uMGUtils, uXYObj,
  uFrmCrossSection, uFrmForces, uFrmSettings, uDM, uFrmTrackEditor,
  uFrmElementEditor, uFrmCollection, bzip2stream, uFrmBouncingEdgePattern,
  uFrmInfo, uFrmResults;

type

  TSoundKind = (
    sndNothing,
    sndBounce,
    sndDrop,
    sndClapping,
    sndApplause,
    sndHit
  );

  TBounceEventKind = (
    beNothing = Integer(sndNothing), // 0
    beBounce = Integer(sndBounce), // 1
    beDrop = Integer(sndDrop) // 2
  );

  TGameEventKind = (
    geNothing = Integer(sndNothing), // 0
    geClapping = Integer(sndClapping), // 1
    geApplause = Integer(sndApplause), // 2
    geHit = Integer(sndHit)
  );

  TAccelerationHistory = array[0..9] of Extended;

  { TFrmMinigolfeeCheck }

  TFrmMinigolfeeCheck = class(TForm)
    btnClear: TButton;
    CD: TOpenDialog;
    imgTerrainSource: TImage;
    imgBackground: TImage;
    imgSource: TImage;
    imgMaskSource: TImage;
    miUnpack: TMenuItem;
    miTools: TMenuItem;
    miInfo: TMenuItem;
    miHelp: TMenuItem;
    miHelpMenu: TMenuItem;
    miReliefCheck: TMenuItem;
    miTrace: TMenuItem;
    miSlowDown: TMenuItem;
    miForces: TMenuItem;
    miChecks: TMenuItem;
    miClear: TMenuItem;
    miDrawTrail: TMenuItem;
    miSep5: TMenuItem;
    miSound: TMenuItem;
    miSep4: TMenuItem;
    MM: TMainMenu;
    miElementEditor: TMenuItem;
    miSep3: TMenuItem;
    miQuit: TMenuItem;
    miSettings: TMenuItem;
    miAllSettings: TMenuItem;
    miClose: TMenuItem;
    miCollection: TMenuItem;
    miTrackEditor: TMenuItem;
    miSep2: TMenuItem;
    miResults: TMenuItem;
    miSep1: TMenuItem;
    miGame: TMenuItem;
    miTrack: TMenuItem;
    miToStart: TMenuItem;
    imgTerrain: TImage;
    MMC: Tplaysound;
    sbrStatus: TStatusBar;
    sbFrame: TScrollBox;
    tmrBouncer: TTimer;
    tmrTimie: TTimer;
    procedure FormActivate(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure imgMaskSourceClick(Sender: TObject);
    procedure imgTerrainClick(Sender: TObject);
    procedure imgTerrainMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure imgTerrainMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure miAllSettingsClick(Sender: TObject);
    procedure miCloseClick(Sender: TObject);
    procedure miCollectionClick(Sender: TObject);
    procedure miDrawTrailClick(Sender: TObject);
    procedure miElementEditorClick(Sender: TObject);
    procedure miHelpClick(Sender: TObject);
    procedure miHelpMenuClick(Sender: TObject);
    procedure miChecksClick(Sender: TObject);
    procedure miForcesClick(Sender: TObject);
    procedure miInfoClick(Sender: TObject);
    procedure miLogoClick(Sender: TObject);
    procedure miQuitClick(Sender: TObject);
    procedure miGameClick(Sender: TObject);
    procedure miReliefCheckClick(Sender: TObject);
    procedure miSettingsClick(Sender: TObject);
    procedure miSoundClick(Sender: TObject);
    procedure miToStartClick(Sender: TObject);
    procedure miTrackClick(Sender: TObject);
    procedure miTrackEditorClick(Sender: TObject);
    procedure miUnpackClick(Sender: TObject);
    procedure tmrBouncerTimer(Sender: TObject);
    procedure SettingsShowForcesChanged;
    procedure SettingsSoundEnabledChanged;
  private
    { private declarations }
    BallPosition: TXY;
    FForce: Single;
    Hit: TXY;
    Starting: Boolean;
    StartXY: TXY;
    DrawJunctionFlag: Boolean;
    MouseCoords: TXY;
    Moving: Boolean;
    FHitCount: LongInt;
    Map: TRelief;
    ContactPointMap: TContactPointMap;
    ContactPoints: T3DContactPointArray;
    Bounce: TBounceEventKind;
    Bounced: Boolean;
    Further: Boolean;
    NewestElevation: Extended;
    AbsElevation: Extended;
    ConPt: TXYLng; // contact point
    PolConPt: TXYLng; // polarised contact point
    BackSavedFlag: Boolean;
    PaintBall_Last_lx: LongInt;
    PaintBall_Last_ly: LongInt;
    AssignContactPoints_LastZ: Variant;
    PlaySound_LastBounce: TBounceEventKind;
    //AssignContactPoints_WasOnEdge: Boolean;
    Slope_LastElevation: Extended;
    Slope_LastSlope: TXY;
    Go_MotionDirection: TXY;
    Go_Vector: TXY;
    imgTerrainMouseUp_MoreTimes: Boolean;
    AccelerationHistory: TAccelerationHistory;
    procedure PaintBall(lx: LongInt = 0; ly: LongInt = 0;
      lw: LongInt = -1; lh: LongInt = -1; Start: Boolean = False);
    procedure RepaintBall;
    function BallCenter: TXY;
    procedure DrawJunction;
    procedure SetForce(AValue: Single);
    procedure SetHitCount(AValue: Integer);
    procedure GoDown;
    procedure Start;
    procedure Go;
    procedure ReturnToStart;
    procedure FitTrack(TerrainWidth, TerrainHeight: Integer);
    procedure AssignContactPoints;
    procedure ViewDirection(Vector: TXY; MaxX: Extended = 0; MaxY: Extended = 0;
      LineColor: TColor = clBlack; First: Boolean = False);
    procedure ViewContactPoints;
    procedure ViewCrossSection;
    procedure PlaySound(OtherType: TGameEventKind = geNothing);
    function Slope(Vector: TXY): TXY;
    function AccelerateDirection(Vector: TXY;
      WhichComponent: TVectorComponent): Extended;
    function BounceDirection(Vector: TXY): TXY;
    function MomentalDirection(Vector: TXY): TXY;
    function CanStop: Boolean; // Warning: This is a dirty hack!
    function TerrainHeightUnderTheBall(X, Y: LongInt): Extended;
    procedure InitializeSettings;
    procedure InitializeAccelerationHistory;
    procedure AddToAccelerationHistory(Acceleration: Extended);
    function CheckMotion: Boolean;
  public
    { public declarations }
    property Force: Single read FForce write SetForce;
    property HitCount: Integer read FHitCount write SetHitCount;
    procedure OpenCompressed(FileName: String);
    procedure OpenUncompressed(FileName: String);
    procedure OpenFromStream(FileStream: TStream; FileName: String);
    procedure Unpack(SourceFile: String);
    function TrackName: String;
  end;


var
  FrmMinigolfeeCheck: TFrmMinigolfeeCheck;

const
  SlopeAcceleration = g_pix_ms2;

  FrictionFactor = 1 / 200;

  MinimalAcceleration = 0.3;

  DefaultTrackWidth = 320;
  DefaultTrackHeight = DefaultTrackWidth;

  msgErrorOpeningFile = 'Error opening file %s.' + LineEnding +
    'Exception class: %s' + LineEnding +
    'Message: "%s"';

  SoundFiles: array[TSoundKind] of String = (
    '',
    'bounce',
    'drop1',
    'clapping',
    'applause',
    'hit'
  );

implementation

{$R *.lfm}

{ TFrmMinigolfeeCheck }

procedure TFrmMinigolfeeCheck.FormClick(Sender: TObject);
begin
  //ShowMessage(PlainFileName(Application.ExeName));
end;

{$HINTS OFF}
procedure TFrmMinigolfeeCheck.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  Moving:=False;
end;
{$HINTS ON}

procedure TFrmMinigolfeeCheck.FormActivate(Sender: TObject);
begin
  SettingsShowForcesChanged;
end;

procedure TFrmMinigolfeeCheck.FormCreate(Sender: TObject);
begin
  InitializeSettings;
  DM.Settings.RegisterShowForcesChangeEvent(@SettingsShowForcesChanged);
  DM.Settings.RegisterSoundEnabledChangeEvent(@SettingsSoundEnabledChanged);
  imgTerrain.AutoSize:=True;
  FitTrack(DefaultTrackWidth, DefaultTrackHeight);
  NewestElevation:=-Infinity;
  imgTerrainMouseUp_MoreTimes:=False;
end;

{$HINTS OFF}
procedure TFrmMinigolfeeCheck.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_HOME:
      begin
        if not miToStart.Enabled then Exit;
        ReturnToStart;
      end;
    VK_CONTROL:
      begin
        if imgTerrain.Enabled then
        begin
          if not Moving then
            Start
          else
          begin
            Moving:=False;
            DrawJunctionFlag:=True;
          end;
        end;
      end;
    VK_SPACE:
      Further:=True;
  end;
end;
{$HINTS ON}

procedure TFrmMinigolfeeCheck.FormShow(Sender: TObject);
var
  PC: Integer;
  FileName: String;
  FileExt: String;
  Command: String;
begin
  PC:=Paramcount;
  case PC of
    1:
      begin
        FileName:=ParamStrUTF8(1);
        FileExt:=LowerCase(ExtractFileExt(FileName));
        if FileExt = txtCollectionExt then
        begin
          Self.Hide;
          FrmTrackEditor.Open(FileName);
        end
        else
          OpenCompressed(FileName);
      end;
    2:
      begin
        Command:=ParamStrUTF8(1);
        if ((Command = '-e')
        or  (Command = '--edit')) then
        begin
          Self.Hide;
          FileName:=ParamStrUTF8(2);
          FrmTrackEditor.Open(FileName);
        end;
      end;
  else
  end;
end;

procedure TFrmMinigolfeeCheck.imgMaskSourceClick(Sender: TObject);
begin

end;

procedure TFrmMinigolfeeCheck.imgTerrainClick(Sender: TObject);
begin

end;

{$HINTS OFF}
procedure TFrmMinigolfeeCheck.imgTerrainMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  MouseCoords := CXY(X, Y);
  if DrawJunctionFlag then
    DrawJunction;
end;
{$HINTS ON}

procedure TFrmMinigolfeeCheck.imgTerrainMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Cheat: Boolean;
begin
  Cheat := DM.Settings.CheatingEnabled;
  if Button = mbLeft then
  begin
    if imgTerrainMouseUp_MoreTimes = False {Releasing the mouse first time} then
    begin
      // Switch the "more times" flag on for the future calls
      imgTerrainMouseUp_MoreTimes:=not imgTerrainMouseUp_MoreTimes;
      HitCount := HitCount + 1;
      Hit.X := X - BallRadius - BallPosition.X;
      Hit.Y := Y - BallRadius - BallPosition.Y;
      Starting := True;
      Start;
    end
    else // Releasing the mouse more than first time
    begin
      if Moving and (Shift = [ssShift]) and Cheat then
      begin
        // Cheatingly stop the ball
        Moving := False;
        DrawJunctionFlag := True;
        MouseCoords := CXY(X, Y);
      end
      else if not Moving then
      begin
        // Start
        HitCount := HitCount + 1;
        Hit.X := X - BallRadius - BallPosition.X;
        Hit.Y := Y - BallRadius - BallPosition.Y;
        Starting := True;
        Start;
      end;
    end;
  end
  else if (Button = mbRight) and Cheat then
  begin
    Moving := False;
    DrawJunctionFlag := False;
    MouseCoords := CXY(X, Y);
    BallPosition := XYShift(MouseCoords, -BallRadius);
    if BallPosition.X < 1 then BallPosition.X := 1;
    if BallPosition.Y < 1 then BallPosition.Y := 1;
    if BallPosition.X > imgTerrain.ClientWidth - BallDiameter - 1 then
      BallPosition.X := imgTerrain.ClientWidth - BallDiameter - 1;
    if BallPosition.Y > imgTerrain.ClientHeight - BallDiameter - 1 then
      BallPosition.Y := imgTerrain.ClientHeight - BallDiameter - 1;
    //PaintBall(trunc(BallPosition.X), trunc(BallPosition.Y));
    RepaintBall; // erase the junction arrow
    DrawJunctionFlag:=True;
    if DM.Settings.ShowForces then
    begin
      AssignContactPoints;
      ViewCrossSection;
      ViewDirection(CXY(), 0, 0, clBlack, True);
      ViewContactPoints;
    end;
  end;
end;

procedure TFrmMinigolfeeCheck.miAllSettingsClick(Sender: TObject);
begin
  FrmSettings.ShowModal;
end;

procedure TFrmMinigolfeeCheck.miCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmMinigolfeeCheck.miCollectionClick(Sender: TObject);
begin
  FrmCollection.Show;
end;

procedure TFrmMinigolfeeCheck.miDrawTrailClick(Sender: TObject);
begin
  ShowMessage('This feature isn’t implemented yet.');
end;

procedure TFrmMinigolfeeCheck.miElementEditorClick(Sender: TObject);
begin
  FrmElementEditor.Show;
end;

procedure TFrmMinigolfeeCheck.miHelpClick(Sender: TObject);
begin

end;

procedure TFrmMinigolfeeCheck.miHelpMenuClick(Sender: TObject);
begin

end;

procedure TFrmMinigolfeeCheck.miChecksClick(Sender: TObject);
begin

end;

procedure TFrmMinigolfeeCheck.miForcesClick(Sender: TObject);
begin
  DM.Settings.ShowForces:=not DM.Settings.ShowForces;
end;

procedure TFrmMinigolfeeCheck.miInfoClick(Sender: TObject);
begin
  FrmInfo.Show;
end;

procedure TFrmMinigolfeeCheck.miLogoClick(Sender: TObject);
begin
end;

procedure TFrmMinigolfeeCheck.miQuitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFrmMinigolfeeCheck.miGameClick(Sender: TObject);
begin

end;

procedure TFrmMinigolfeeCheck.miReliefCheckClick(Sender: TObject);
var
  X, Y, I: LongInt;
  {.$DEFINE RELIEFLOG}
  {$IFDEF RELIEFLOG}
  CheckList: TStringList;
  {$ENDIF}
begin
  if not (ContactPointMap=nil) then
  begin
    try
      try
        // Check plain relief map
        for X:=0 to imgTerrain.ClientWidth - 1 do
        begin
          for Y:=0 to imgTerrain.ClientHeight - 1 do
            if Map[Y, X] = 0 then
            begin
              imgTerrain.Canvas.Pen.Color:=clFuchsia;
              imgTerrain.Canvas.Line(X, Y, X + 1, Y + 1);
            end;
          imgTerrain.Repaint;
        end;
        // Check contact point map
        {$IFDEF RELIEFLOG}
        CheckList := TStringList.Create;
        {$ENDIF}
        for I:=0 to High(ContactPointMap) do
        begin
          for Y:=0 to imgTerrain.ClientHeight - BallDiameter - 1 do
          begin
            for X:=0 to imgTerrain.ClientWidth - BallDiameter - 1 do
            begin
              if  (ContactPointMap[0, Y, X].X = 7)
              and (ContactPointMap[0, Y, X].Y = 7) then
                imgTerrain.Canvas.Pixels[X + 7, Y + 7]:=clBlack;
              {$IFDEF RELIEFLOG}
              CheckList.Append(Format('X=%04d, Y=%04d, I=%d: {%02d, %02d}',
                [X, Y, I,
                ContactPointMap[I, Y, X].X,
                ContactPointMap[I, Y, X].Y]
              ));
              {$ENDIF}
            end;
            imgTerrain.Repaint;
          end;
        end;
      except
        on E: Exception do
        begin
          ShowMessage('An error occured during relief check.' + LineEnding +
            'Exception class: ' + E.ClassName + LineEnding +
            'Message: “' + E.Message + '”' + LineEnding +
            'X: ' + IntToStr(X) + LineEnding +
            'Y: ' + IntToStr(Y));
        end;
      end;
    finally
      {$IFDEF RELIEFLOG}
      CheckList.SaveToFile(UTF8ToSys(CD.FileName + '.log'));
      {$ENDIF}
    end;
  end;
end;

procedure TFrmMinigolfeeCheck.miSettingsClick(Sender: TObject);
begin

end;

procedure TFrmMinigolfeeCheck.miSoundClick(Sender: TObject);
var
  SoundEnabled: Boolean;
begin
  SoundEnabled:=DM.Settings.SoundEnabled;
  DM.Settings.SoundEnabled := not SoundEnabled;
end;

procedure TFrmMinigolfeeCheck.miToStartClick(Sender: TObject);
begin
  ReturnToStart;
end;

procedure TFrmMinigolfeeCheck.miTrackClick(Sender: TObject);
var
  OpenFlag: Boolean;
  FileName: String;
  Ext: String;
begin
  CD.InitialDir:=ExtractFilePath(Application.ExeName) + DirectorySeparator
    + txtTrackSubdir;
  CD.Title:='Choose a track';
  CD.Filter:='MinigolfeeCheck Tracks (*' + txtTrackExt + ')|*' +
    txtTrackExt + '|' +
    'MinigolfeeCheck Uncompressed Tracks (*' + txtUncompressedTrackExt +
    ')|*' + txtUncompressedTrackExt;
  CD.DefaultExt:=txtTrackExt;
  CD.Options:=[ofFileMustExist, ofHideReadOnly];
  OpenFlag:=CD.Execute;
  FileName:=CD.FileName;
  Ext:=ExtractFileExt(FileName);
  if OpenFlag then
  begin
    if Ext = txtTrackExt then
      OpenCompressed(FileName)
    else if Ext = txtUncompressedTrackExt then
      OpenUncompressed(FileName);
  end
  else
    Self.Enabled:=True;
end;

procedure TFrmMinigolfeeCheck.miTrackEditorClick(Sender: TObject);
begin
  FrmTrackEditor.Show;
end;

procedure TFrmMinigolfeeCheck.miUnpackClick(Sender: TObject);
var
  UnpackFlag: Boolean;
  FileName: String;
begin
  CD.InitialDir:=ExtractFilePath(Application.ExeName) + DirectorySeparator
    + txtTrackSubdir;
  CD.Title:='Choose a track to unpack';
  CD.Filter:='MinigolfeeCheck Tracks (*' + txtTrackExt + ')|*' +
    txtTrackExt;
  CD.DefaultExt:=txtTrackExt;
  CD.Options:=[ofFileMustExist, ofHideReadOnly];
  UnpackFlag:=CD.Execute;
  FileName:=CD.FileName;
  if UnpackFlag then
  begin
    Unpack(FileName);
    Beep;
  end;
end;

procedure TFrmMinigolfeeCheck.SettingsShowForcesChanged;
var
  ShowForces: Boolean;
begin
  ShowForces:=DM.Settings.ShowForces;
  miForces.Checked:=ShowForces;
  FrmForces.Visible:=ShowForces;
  FrmCrossSection.Visible:=ShowForces;
  if ShowForces then
  begin
    // Place the forces form
    FrmForces.Left:=left - FrmForces.Width;
    FrmForces.Top:=Top;
    // Place the cross-section form
    FrmCrossSection.Left:=Left - FrmCrossSection.Width;
    FrmCrossSection.Top:=FrmForces.BoundsRect.Bottom;
    // Paint contact points, forces & cross-section
    AssignContactPoints;
    ViewContactPoints;
    ViewCrossSection;
  end;
end;

procedure TFrmMinigolfeeCheck.SettingsSoundEnabledChanged;
begin
  miSound.Checked:=DM.Settings.SoundEnabled;
end;

procedure TFrmMinigolfeeCheck.tmrBouncerTimer(Sender: TObject);
begin
  Bounced:=False;
  tmrBouncer.Enabled:=False;
end;

procedure TFrmMinigolfeeCheck.PaintBall(lx: LongInt; ly: LongInt; lw: LongInt;
  lh: LongInt; Start: Boolean);
var
  DrawingRect: TRect;
  procedure CopyRect(SrcImage, DstImage: TImage; SrcRect, DstRect: TRect;
    CopyMode: TCopyMode);
  begin
    DstImage.Canvas.CopyMode:=CopyMode;
    DstImage.Canvas.CopyRect(DstRect, SrcImage.Canvas, SrcRect);
  end;
  procedure LoadRect(SrcImage: TImage; DstRect: TRect;
    CopyMode: TCopyMode = cmSrcCopy);
  begin
    CopyRect(SrcImage, imgTerrain, SrcImage.ClientRect, DstRect, CopyMode);
  end;
  procedure SaveRect(SrcRect: TRect);
  begin
    CopyRect(imgTerrain, imgBackground, SrcRect, imgBackground.ClientRect,
      cmSrcCopy);
  end;
begin
  if lw = -1 then lw:=imgBackground.Picture.Bitmap.Width;
  if lh = -1 then lh:=imgBackground.Picture.Bitmap.Height;
  // If we are starting, the background isn't saved!
  if Start then BackSavedFlag:=False;
  // If we are running, the background is saved so bring it back!
  if BackSavedFlag = True then
  begin
    DrawingRect:=Rect(PaintBall_Last_lx, PaintBall_last_ly,
      PaintBall_Last_lx + lw, PaintBall_Last_ly + lh);
    LoadRect(imgBackground, DrawingRect);
  end;
  // Draws a dot to check the trace
  if miDrawTrail.Checked then // If it's allowed
    imgTerrain.Picture.Bitmap.Canvas.Pixels[lx + 7, ly + 7]:=
      RGBToColor($FF, $00, $FF);
  // Saves the backround from the new position
  DrawingRect:=Rect(lx, ly, lx + lw, ly + lh);
  SaveRect(DrawingRect);
  // Saved.
  BackSavedFlag:=True;
  // Applies mask
  // DrawingRect remains the same
  LoadRect(imgMaskSource, DrawingRect, cmSrcAnd);
  // Draws the ball
  // DrawingRect remains the same
  LoadRect(imgSource, DrawingRect, cmSrcPaint);
  //imgTerrain.Repaint;
  // Remembers this position for the next round
  PaintBall_Last_lx:=lx;
  PaintBall_Last_ly:=ly;
end;

procedure TFrmMinigolfeeCheck.RepaintBall;
begin
  imgTerrain.Canvas.CopyMode:=cmSrcCopy;
  imgTerrain.Canvas.CopyRect(ClientRect, imgTerrainSource.Canvas, ClientRect);
  PaintBall(round(BallPosition.X), round(BallPosition.Y),
    imgSource.ClientWidth, imgSource.ClientHeight, True);
end;

function TFrmMinigolfeeCheck.BallCenter: TXY;
begin
  Result:=XYShift(BallPosition, 7);
end;

procedure TFrmMinigolfeeCheck.DrawJunction;
var
  BeakLength: Extended;
  Junction: TXYObj;
  Goal: TXYObj;
  locBallCenter: TXY;
  GoalEdge: TXY;
  I: Integer;
  Lines: array[0..1] of TXYObj;
begin
  if DrawJunctionFlag then
  begin
    with imgTerrain do
    begin
      Goal:=MouseCoords;
      locBallCenter:=BallCenter;
      Junction:=Goal - locBallCenter;
      if not DM.Settings.AimWithSling then
        Junction.Angle:=Junction.Angle + 180;
      Force:=Junction.Length;
      if (Junction.Length > 7) and DM.Settings.AimWithSling then
        Junction.Length:=Junction.Length - 7;
      GoalEdge:=locBallCenter + CXY(Junction);
      BeakLength:=Junction.Length / 5;
      RepaintBall;
      Canvas.Pen.Width:=DM.Settings.ArrowWidth;
      Canvas.Pen.Color:=DM.Settings.ArrowBodyColor;
      Canvas.CopyMode:=cmSrcCopy;
      Canvas.Line(round(locBallCenter.X), round(locBallCenter.Y),
        round(GoalEdge.X), round(GoalEdge.Y));
      for I:=0 to 1 do
      begin
        Lines[I]:=TXYObj.Create();
        Lines[I].Length:=BeakLength;
        Lines[I].Angle:=Junction.Angle + (360 - (I * 2 - 1) * 30);
        Canvas.Pen.Color:=DM.Settings.ArrowHeadColor;
        Canvas.Line(
          round(GoalEdge.X - Lines[I].X),
          round(GoalEdge.Y - Lines[I].Y),
          round(GoalEdge.X),
          round(GoalEdge.Y)
        );
      end; // for
      Canvas.AutoRedraw:=True;
      Application.ProcessMessages;
    end; // with
  end; // if
end;

procedure TFrmMinigolfeeCheck.SetForce(AValue: Single);
begin
  if FForce=AValue then Exit;
  FForce:=AValue;
  sbrStatus.Panels[0].Text:='Force: ' + FloatToStr(AValue);
end;

procedure TFrmMinigolfeeCheck.SetHitCount(AValue: Integer);
begin
  sbrStatus.Panels[2].Text:='Hit count: ' + IntToStr(AValue);
  FHitCount:=AValue;
end;

procedure TFrmMinigolfeeCheck.GoDown;
begin
  Starting:=True;
  Hit:=CXY(0, 0);
  FrmForces.miControlField.Caption:=txtControlField;
  DrawJunctionFlag:=False;
  Moving:=False;
  Go;
  MouseCoords:=BallCenter;
end;

procedure TFrmMinigolfeeCheck.Start;
begin
  Starting:=True;
  Hit:=XYDiff(MouseCoords, BallCenter);
  if not DM.Settings.AimWithSling then Hit:=XYNeg(Hit);
  InitializeAccelerationHistory;
  FrmForces.miControlField.Caption:=txtControlField;
  DrawJunctionFlag:=False;
  Moving:=False;
  RepaintBall; // Erase junction
  Go;
end;

procedure TFrmMinigolfeeCheck.Go;
var
  //t, v: Extended;
  X, Y{, Z}: Extended;
  locSlope: TXY;
  objXY: TXYObj;
  HowManyTimesItRan: LongInt;
  PlayerName: String;
  locTrackName: String;
  RecordDate: TDateTime;
begin
  Moving:=True;
  tmrTimie.Enabled:=DM.Settings.Timer;
  HowManyTimesItRan:=0;
  while Moving do
  begin
    Application.ProcessMessages;

    if Starting then
    begin
      if (HitCount > 0) and DM.Settings.SoundEnabled then
      begin
        PlaySound(geHit);
      end;
      X := Hit.X / 25;
      Y := Hit.Y / 25;
      Starting:=False;
      //NewestElevation := -$FFFFFF; // so that it wouldn't think it fell down.
    end;

    AssignContactPoints; { gets the points from the contact points map
                           where do the forces act from }
    if AbsElevation = GoalHeight + BallRadius {goal reached} then
    begin
      //Moving:=False;
      tmrTimie.Enabled:=False;
      Application.ProcessMessages;
      Break;
    end;

    locSlope := Slope(CXY(X, Y));
    // Simulate friction
    X := locSlope.X - locSlope.X * FrictionFactor;
    Y := locSlope.Y - locSlope.Y * FrictionFactor;
    Go_Vector := CXY(X, Y);
    Go_MotionDirection := MomentalDirection(Go_Vector);

    if not Moving then Break;
    BallPosition += Go_MotionDirection;

    AssignContactPoints;

    // If the ball is hardly moving, stop it
    objXY := CXYObj(X, Y);
    AddToAccelerationHistory(objXY.Length);
    if not CheckMotion then
    begin
      if CanStop then
      begin
        Moving := False;
        tmrTimie.Enabled := False;
        DrawJunctionFlag := True;
        Break;
      end;
    end;
    objXY.Free;

    // Play a sound according to the Bounce variable
    if DM.Settings.SoundEnabled then PlaySound();

    // View forces
    if DM.Settings.ShowForces then
    begin
      //AssignContactPoints;
      ViewDirection(Go_Vector, 200, 200, clWhite, True);
      ViewDirection(MomentalDirection(Go_Vector), 1, 1, clBlue);
      ViewDirection(locSlope, 5, 5, clFuchsia);
      ViewContactPoints;
      ViewCrossSection;
      Further := not DM.Settings.TraceMode;
      while not Further do
        Application.ProcessMessages;
    end;

    Inc(HowManyTimesItRan);
    if HowManyTimesItRan = DM.Settings.BallVelocity then
    begin
      if not DM.Settings.Timer then
        PaintBall(trunc(BallPosition.X), trunc(BallPosition.Y));
      HowManyTimesItRan:=0;
    end;
  end;
  PaintBall(trunc(BallPosition.X), trunc(BallPosition.Y));
  Application.ProcessMessages;
  if HitCount > 0 then DrawJunction;
  // If the goal gets reached:
  if NewestElevation = GoalHeight + BallRadius then
  begin
    tmrTimie.Enabled := False;
    Application.ProcessMessages;
    PlayerName := InputBox(
      'Results',
      Format(
        'Congratulations, you have just reached the goal!' + LineEnding +
        'Hit count: %d.' + LineEnding +
        'Enter your name into the winners record, please!',
        [HitCount]
      ),
      DM.Settings.LastPlayerName
    );
    DM.Settings.LastPlayerName := PlayerName;
    if PlayerName = '' then PlayerName := 'Anonymous';
    { TODO : Finish FrmResults }
    locTrackName := TrackName;
    RecordDate := Now();
    FrmResults.WriteResult(locTrackName, PlayerName, HitCount, RecordDate);
    if DM.Settings.SoundEnabled then
    begin
      if HitCount <= 7 then
        PlaySound(geApplause)
      else
        PlaySound(geClapping);
    end;
    FrmResults.ShowResults(locTrackName, PlayerName, HitCount, RecordDate);
  end;
end;

procedure TFrmMinigolfeeCheck.ReturnToStart;
begin
  Moving:=False;
  tmrTimie.Enabled:=False;
  BallPosition:=StartXY;
  PaintBall(round(BallPosition.X), round(BallPosition.Y));
  DrawJunctionFlag:=True;
  HitCount:=0;
  if not DM.Settings.AnchorBall then GoDown;
end;

procedure TFrmMinigolfeeCheck.FitTrack(TerrainWidth, TerrainHeight: Integer);
var
  MapWidth, MapHeight: Integer;
begin
  MapWidth:=TerrainWidth + 2; // 2 pixels for a border
  MapHeight:=TerrainHeight + 2; // -"-
  imgTerrain.Left:=-1; // 1 pixel for a border
  imgTerrain.Top:=-1; // -"-
  imgTerrain.ClientWidth:=MapWidth;
  imgTerrain.ClientHeight:=MapHeight;
  sbFrame.ClientWidth:=TerrainWidth;
  sbFrame.ClientHeight:=TerrainHeight;
  Self.BorderStyle:=bsSizeable;
  Self.ClientWidth:=sbFrame.Width;
  Self.ClientHeight:=sbFrame.Height + sbrStatus.Height;
  Self.BorderStyle:=bsSingle;
end;

procedure TFrmMinigolfeeCheck.AssignContactPoints;
var
  I: LongInt;
  IPX, IPY: Integer; // Integer position X and Y
  //Order: Variant;
  ContactPoint: TXYLngZVar;
begin
  IPX:=trunc(BallPosition.X);
  IPY:=trunc(BallPosition.Y);
  try
    SetLength(ContactPoints, 0);
    // Avoid empty contact point map
    if not (ContactPointMap = nil) then
    begin
      if (IPX >= 0) and (IPX <= High(Map[0]) - BallDiameter + 1) and
        (IPY >= 0) and (IPY <= High(Map) - BallDiameter + 1) then
      begin
        for I:=0 to High(ContactPointMap) do
        begin
          if (ContactPointMap[I, IPY, IPX].X = 0)
          and (ContactPointMap[I, IPY, IPX].Y = 0) then
          begin
            Assert(not (I = 0), 'Zero contact point with nonzero index ' +
              'caught!');
            Break;
          end;
          SetLength(ContactPoints, I + 1);
          ContactPoint:=ContactPoints[I];
          ContactPoint.X:=ContactPointMap[I, IPY, IPX].X;
          ContactPoint.Y:=ContactPointMap[I, IPY, IPX].Y;
          ContactPoint.Z:=
            Map[IPY + ContactPoint.Y, IPX + ContactPoint.X] +
            BallMap[ContactPoint.X, ContactPoint.Y];
          ContactPoints[I]:=ContactPoint; // We assign the point data back
                                          // into the array
          if not FrmBouncingEdgePattern.OnEdge(ContactPoints[I]) then
          begin
            NewestElevation:=ContactPoint.Z;
            AssignContactPoints_LastZ:=NewestElevation;
            AbsElevation:=NewestElevation;
          end
          else // on the edge
          begin
            // Don't change the newest elevation
            AbsElevation:=ContactPoint.Z;
          end; // if
        end; // for
      end
      else // out of map
      begin
        ShowMessage('The ball has just gotten out of the map!' + LineEnding +
          'This is an error, please be so kind to contact the author.');
        // Go to the start
        Moving:=False;
        DrawJunctionFlag:=True;
        BallPosition:=StartXY;
        AssignContactPoints; // Recurse
      end; // if - range check
    end; // if
  except
    on E: Exception do
    begin
      ShowMessage('Invalid contact points map!' + LineEnding +
        'Exception class: ' + E.ClassName + LineEnding +
        'Message: ' + E.Message);
    end; // on
  end; // try
end;

procedure TFrmMinigolfeeCheck.ViewDirection(Vector: TXY; MaxX: Extended;
  MaxY: Extended; LineColor: TColor; First: Boolean);
begin
  FrmForces.DrawDirection(Vector, MaxX, MaxY, LineColor, First);
end;

procedure TFrmMinigolfeeCheck.ViewContactPoints;
begin
  FrmForces.ContactPoints:=ContactPoints;
end;

procedure TFrmMinigolfeeCheck.ViewCrossSection;
var
  X: LongInt;
  MaxZ: Extended;
  PointToDraw: TPoint;
  BallCenterToDraw: TXY;
begin
  with FrmCrossSection do
  begin
    Clear;
    for X := round(ScaleLeft) to round(ScaleLeft + ScaleWidth) do
    begin
      PointToDraw := Point(
        round(BallPosition.X) + BallRadius + X,
        round(BallPosition.Y) + BallRadius
      );
      // Let's check for the ranges
      if (PointToDraw.Y >= 0) and
        (PointToDraw.Y <= High(Map)) and
        (PointToDraw.X >= 0) and
        (PointToDraw.X <= High(Map[0])) then
      begin
        // Draw a vertical line, a part of the terrain
        MaxZ := Map[PointToDraw.Y, PointToDraw.X];
        imgCrossSection.Canvas.Pen.Width:=round(ClientWidth / ScaleWidth) + 1;
        ScaleLine(CXY(X, 0), CXY(X, MaxZ));
      end;
    end;

    BallCenterToDraw.X := 0;
    if NewestElevation = -Infinity then
      BallCenterToDraw.Y := BallRadius
    else
      BallCenterToDraw.Y := NewestElevation;
    imgCrossSection.Canvas.Pen.Width:=1;
    ScaleCircle(BallCenterToDraw, BallRadius);
  end;
end;

procedure TFrmMinigolfeeCheck.PlaySound(OtherType: TGameEventKind);
  procedure DoPlaySound(SoundType: TSoundKind);
  begin
    MMC.SoundFile:=SoundPath(SoundFiles[SoundType]);
    MMC.Execute;
  end;
begin
  case OtherType of
  geNothing: // redirect to the original sound
    begin
      case Bounce of
      beNothing: {do nothing};
      else
        Application.ProcessMessages;
        if Bounce <> PlaySound_LastBounce then
        begin
          DoPlaySound(TSoundKind(Bounce));
        end;
      end;
    end;
  else
    DoPlaySound(TSoundKind(OtherType));
  end;
  PlaySound_LastBounce:=Bounce;
  if Bounce > beNothing then
    Application.ProcessMessages;
end;

function TFrmMinigolfeeCheck.Slope(Vector: TXY): TXY;
var
  Slant: TXY;
  I: LongInt;
  YIsSet: Boolean;
  BounceIndicatorX, BounceIndicatorY: LongInt;
begin
  // Initialise the result
  Result := CXY();
  // If the ball didn't sit down well for any reason:
  if Length(ContactPoints) > 1 then
  begin
    if (ContactPoints[0].X = 0) and (ContactPoints[0].Y = 0) then
    begin
      // Keep the last slope.
      Result := Vector;
      Exit;
    end;
  end;
  for I := 0 to High(ContactPoints) do
  begin
    if (ContactPoints[I].X = 0) and (ContactPoints[I].Y = 0) then Break;
    PolConPt := CXYLng(
      ContactPoints[I].X - BallRadius, ContactPoints[I].Y - BallRadius
    );
    ConPt := CXYLng(PolConPt.X + BallRadius, PolConPt.Y + BallRadius);
    YIsSet := False;

    // The bounce depends on the direction and the contact point
    // Selection procedure on the X-axis
    BounceIndicatorX := PolConPt.X * Sign(Vector.X);
    if BounceIndicatorX = 0 then
      // No bounce
      Slant.X := AccelerateDirection(Vector, vcX)
    else if BounceIndicatorX < 0 then
    begin
      if FrmBouncingEdgePattern.OnEdge(ConPt) then
      begin
        { Don't accelerate - maybe it was a bounce
        (the previous falling has already accelerated you) }
        Slant.X := Vector.X;
        if Bounce <> beBounce then Bounce := beDrop;
      end
      else
        // Acceleration on a slope
        Slant.X := AccelerateDirection(Vector, vcX);
    end
    else if BounceIndicatorX > 0 then
    begin
      if FrmBouncingEdgePattern.OnEdge(ConPt) then
      begin
        // Absolute bounce
        Bounce := beBounce;
        if PolConPt.Y = 0 then
          Slant.X := -1 * Vector.X
        else
        begin
          // (X) (The bounce will be changed as needed)
          Slant := BounceDirection(Vector);
          YIsSet := True;
        end;
      end
      else
        // Slowdown
        Slant.X := AccelerateDirection(Vector, vcX);
    end;

    // Selection procedure on the Y-axis
    BounceIndicatorY := PolConPt.Y * Sign(Vector.Y);
    if BounceIndicatorY = 0 then
    begin
      // No bounce
      if not YIsSet then Slant.Y := AccelerateDirection(Vector, vcY);
    end
    else if BounceIndicatorY < 0 then
    begin
      if FrmBouncingEdgePattern.OnEdge(ConPt) then
      begin
        { Don't accelerate - maybe it was a bounce
        (the previous falling has already accelerated you) }
        if not YIsSet then Slant.Y := Vector.Y;
        if Bounce <> beBounce then Bounce := beDrop;
      end
      else
        // Acceleration
        Slant.Y := AccelerateDirection(Vector, vcY);
    end
    else if BounceIndicatorY > 0 then
    begin
      if FrmBouncingEdgePattern.OnEdge(ConPt) then
      begin
        // Absolute bounce
        Bounce := beBounce;
        if PolConPt.X = 0 then
          Slant.Y := -1 * Vector.Y
        else
          // (Y) (The bounce will be changed as needed)
          Slant := BounceDirection(Vector);
      end
      else
        // Slowdown
        Slant.Y := AccelerateDirection(Vector, vcY);
    end;

    Result.X += Slant.X;
    Result.Y += Slant.Y;
    Vector := Slant;
  end;

  Slant := Vector;

  Slope_LastElevation := NewestElevation;
  Slope_LastSlope := Result;
end;

function TFrmMinigolfeeCheck.AccelerateDirection(Vector: TXY;
  WhichComponent: TVectorComponent): Extended;
begin
  case WhichComponent of
  vcX:
    Result := Vector.X + SlopeAcceleration * (-PolConPt.X);
  vcY:
    Result := Vector.Y + SlopeAcceleration * (-PolConPt.Y);
  else
    raise Exception.Create('Wrong direction component passed to ' +
      'AccelerateDirection.');
  end;
end;

function TFrmMinigolfeeCheck.BounceDirection(Vector: TXY): TXY;
var
  ArcusCadendi: Extended;
  VectorAxalis: TXY;
begin
  VectorAxalis := CXY(PolConPt);
  ArcusCadendi := ArcusAbAxe(Vector, VectorAxalis);
  if (ArcusCadendi >= 90) and (ArcusCadendi <= 270) then
  begin
    Result := Vector;
    if Bounce <> beDrop then Bounce := beBounce;
  end
  else
  begin
    if Sign(PolConPt.X) = Sign(PolConPt.Y) then
    begin
      Result.X := -Vector.Y;
      Result.Y := -Vector.X;
    end
    else
    begin
      Result.X := Vector.Y;
      Result.Y := Vector.X;
    end;
    Bounce := beBounce;
  end;
end;

function TFrmMinigolfeeCheck.MomentalDirection(Vector: TXY): TXY;
var
  objVector: TXYObj;
begin
  objVector := TXYObj.Create(Vector);
  if objVector.Length > 1 then objVector.Length := 1;
  Result := CXY(objVector);
end;

function TFrmMinigolfeeCheck.CanStop: Boolean;
var
  I: LongInt;
begin
  // Warning: This is a dirty hack!
  for I := 0 to High(ContactPoints) do
  begin
    if  (
          (ContactPoints[I].X = BallRadius)
          and
          (ContactPoints[I].Y = BallRadius)
        )
        or
        (Length(ContactPoints) > 1)
        or // query this terrain shape on the X-axis: -__-
        (
          (
            TerrainHeightUnderTheBall(BallRadius, BallRadius)
            =
            TerrainHeightUnderTheBall(BallRadius, BallRadius + 1)
          )
          and
          (
            TerrainHeightUnderTheBall(BallRadius, BallRadius - 1)
            =
            TerrainHeightUnderTheBall(BallRadius, BallRadius + 2)
          )
          and
          (
            TerrainHeightUnderTheBall(BallRadius, BallRadius - 1)
            >
            TerrainHeightUnderTheBall(BallRadius, BallRadius)
          )
          and
          (
            NewestElevation - BallRadius
            >
            TerrainHeightUnderTheBall(BallRadius, BallRadius)
          )
        )
        or // query this terrain shape on the Y-axis: -__-
        (
          (
            TerrainHeightUnderTheBall(BallRadius, BallRadius)
            =
            TerrainHeightUnderTheBall(BallRadius + 1, BallRadius)
          )
          and
          (
            TerrainHeightUnderTheBall(BallRadius - 1, BallRadius)
            =
            TerrainHeightUnderTheBall(BallRadius + 2, BallRadius)
          )
          and
          (
            TerrainHeightUnderTheBall(BallRadius, BallRadius - 1)
            >
            TerrainHeightUnderTheBall(BallRadius, BallRadius)
          )
          and
          (
            NewestElevation - BallRadius
            >
            TerrainHeightUnderTheBall(BallRadius, BallRadius)
          )
        )
        or
        FrmBouncingEdgePattern.OnEdge(ContactPoints[I])
        then
          Result := True;
  end;
end;

function TFrmMinigolfeeCheck.TerrainHeightUnderTheBall(X, Y: LongInt): Extended;
var
  PointLiesUnderTheBall: Boolean;
begin
  PointLiesUnderTheBall:=InBall(X, Y);
  if PointLiesUnderTheBall then
    Result := Map[round(BallPosition.Y) + Y, round(BallPosition.X) + X]
  else
    Result := OutOfBall;
end;

procedure TFrmMinigolfeeCheck.InitializeSettings;
begin
  miSound.Checked:=DM.Settings.SoundEnabled;
end;

procedure TFrmMinigolfeeCheck.InitializeAccelerationHistory;
var
  I: Integer;
begin
  for I := Low(AccelerationHistory) to High(AccelerationHistory) do
    AccelerationHistory[I] := MinimalAcceleration;
end;

procedure TFrmMinigolfeeCheck.AddToAccelerationHistory(Acceleration: Extended);
var
  I: Integer;
begin
  for I := High(AccelerationHistory) downto Succ(Low(AccelerationHistory)) do
    AccelerationHistory[I] := AccelerationHistory[I - 1];
  AccelerationHistory[0] := Acceleration;
end;

function TFrmMinigolfeeCheck.CheckMotion: Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := High(AccelerationHistory) downto Low(AccelerationHistory) do
    Result := Result or (AccelerationHistory[I] >= MinimalAcceleration);
end;

procedure TFrmMinigolfeeCheck.OpenCompressed(FileName: String);
var
  FileStream: TFileStream;
  BZ2Stream: TDecompressBzip2Stream;
begin
  try
    FileStream:=TFileStream.Create(UTF8ToSys(FileName), fmOpenRead);
    BZ2Stream:=TDecompressBzip2Stream.Create(FileStream);
    OpenFromStream(BZ2Stream, FileName);
  except
    on E: Exception do
      ShowMessageFmt(
        msgErrorOpeningFile,
        [FileName, E.ClassName, E.Message]
      );
  end;
end;

procedure TFrmMinigolfeeCheck.OpenUncompressed(FileName: String);
var
  FileStream: TFileStream;
begin
  try
    FileStream:=TFileStream.Create(LazUTF8.UTF8ToSys(FileName), fmOpenRead);
    OpenFromStream(FileStream, FileName);
  except
    on E: Exception do
      ShowMessageFmt(
        msgErrorOpeningFile,
        [FileName, E.ClassName, E.Message]
      );
  end;
end;

procedure TFrmMinigolfeeCheck.OpenFromStream(FileStream: TStream;
  FileName: String);
var
  TextureName: String;
  TerrainWidth, TerrainHeight: SmallInt;
  MapWidth, MapHeight: SmallInt;
  MaxContactPointIndex: SmallInt;
  TextureGraphic: TPortableNetworkGraphic;
  TextureBitmap: TBitmap;
  //ExtentX, ExtentY: Byte;
  Y, I: SmallInt;
begin
  Self.Cursor:=crHourGlass;
  //ShowMessage(FileName);
  imgTerrain.Enabled:=False;
  imgTerrain.Cursor:=crHourGlass;
  Moving:=False;
  TextureName:=PlainFileName(FileName) + txtBitmapExt;
  try
    try
      TerrainWidth:=FileStream.ReadWord;
      TerrainHeight:=FileStream.ReadWord;
      // 1-pixel border on all sides -> add 2 to width and height
      MapWidth:=TerrainWidth + 2;
      MapHeight:=TerrainHeight + 2;
      {ExtentX:=}FileStream.ReadByte;
      {ExtentY:=}FileStream.ReadByte;
      MaxContactPointIndex:=FileStream.ReadWord;
      FileStream.ReadBuffer(StartXY, SizeOf(StartXY));
      SetLength(Map, MapHeight, MapWidth);
      for Y:=0 to MapHeight - 1 do
        FileStream.ReadBuffer(Map[Y, 0], SizeOf(Map[Y, 0]) * Length(Map[Y]));
      SetLength(
        ContactPointMap,
        MaxContactPointIndex + 1,
        MapHeight - BallDiameter + 1,
        MapWidth - BallDiameter + 1
      );
      for I:=0 to MaxContactPointIndex do
      begin
        for Y:=0 to MapHeight - BallDiameter do
        begin
          FileStream.ReadBuffer(ContactPointMap[I][Y][0],
            SizeOf(ContactPointMap[I][Y][0]) * Length(ContactPointMap[I][Y]));
        end;
      end;
      Moving:=False;
      tmrTimie.Enabled:=False;
      BallPosition:=StartXY;

      TextureGraphic := TPortableNetworkGraphic.Create;
      TextureGraphic.LoadFromFile(TextureName);
      TextureBitmap := TBitmap.Create;
      TextureBitmap.Assign(TextureGraphic);
      imgTerrain.Picture.Bitmap := TextureBitmap;
      imgTerrainSource.Picture.Bitmap := TextureBitmap;

      // Resize imgTerrain, sbFrame and form accordingly
      FitTrack(TerrainWidth, TerrainHeight);

      PaintBall(trunc(BallPosition.X), trunc(BallPosition.Y), -1, -1, True);
      DrawJunctionFlag:=True;
      HitCount:=0;

      Self.Caption:='MinigolfeeCheck – ' + ExtractFileNameOnly(FileName);

      if DM.Settings.ShowForces then
      begin
        AssignContactPoints;
        ViewDirection(CXY(), 0, 0, clBlack, True);
        ViewContactPoints;
        ViewCrossSection;
      end;

      if not DM.Settings.AnchorBall then GoDown;

      miToStart.Enabled:=True;
      miResults.Enabled:=True;
      CD.FileName:=FileName;
    except
      on E: Exception do
      begin
        ShowMessage('An error has occured while opening the file "' + FileName +
          '".' + LineEnding + 'Exception class: ' + E.ClassName + LineEnding +
          'Message: ' + E.Message);
        FileStream.Free;
        Self.Enabled:=True;
        imgTerrain.Enabled:=True;
        Self.Cursor:=crDefault;
        imgTerrain.Cursor:=crCross;
      end;
    end;
  finally
    FileStream.Free;
    //Self.Enabled:=True;
    imgTerrain.Enabled:=True;
    Self.Cursor:=crDefault;
    imgTerrain.Cursor:=crCross;
  end;
end;

procedure TFrmMinigolfeeCheck.Unpack(SourceFile: String);
var
  InStream: TFileStream;
  Decompressed: TDecompressBzip2Stream;
  OutStream: TFileStream;
  Buffer: Pointer;
  I: integer;
  SourceFileSys: String;
  TargetFile: String;
  TargetFileSys: String;
const BufferSize=$2000;
begin
  SourceFileSys := UTF8ToSys(SourceFile);
  TargetFile := ExtractFileNameWithoutExt(SourceFile) + txtUncompressedTrackExt;
  TargetFileSys := UTF8ToSys(TargetFile);
  InStream := TFileStream.Create(SourceFileSys, fmOpenRead);
  try
    try
      Decompressed := TDecompressBzip2Stream.Create(InStream);
    except
      // Something went wrong, e.g. invalid format
      // Now get out of function with result false
      Exit;
    end;
    OutStream := TFileStream.Create(TargetFileSys, fmCreate);
    try
      // We don't have seek on the TDecompressBzip2stream, so can't use
      // CopyFrom...
      // Decompressed.CopyFrom(InStream, InStream.Size);
      GetMem(Buffer, BufferSize);
      repeat
        I := Decompressed.Read(buffer^, BufferSize);
        if I > 0 then
          OutStream.WriteBuffer(Buffer^, I);
      until I < BufferSize;
    finally
      Decompressed.Free;
      OutStream.Free;
    end;
  finally
    InStream.Free;
  end;
end;

function TFrmMinigolfeeCheck.TrackName: String;
begin
  Result := LazFileUtils.ExtractFileNameOnly(CD.FileName);
end;


end.

