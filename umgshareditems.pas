unit uMGSharedItems;

{$mode objfpc}{$H+}

interface

uses Classes, SysUtils, Variants, Controls, {Math,} Forms, uBuffer,
  Graphics, uFrmBouncingEdgePattern{, uMGBasicTypes, uXYObj}, VersionSupport;

const
  BallDiameter = 15;
  BallRadius = 7;
  {.$Include version.inc}

var
  SharedElementMap: array of array of Single;
  BallMap: array[0..BallDiameter - 1, 0..BallDiameter - 1] of Extended;
  Buffer: TBuffer;

const
  PPM = 3779.5242;
  G = 9.81; // gravitanional constant
  //g_pix_ms2 = g * PPM * (1000 ^ -2) ' gravitational constant
            // in pixels per squared millisecond
  g_pix_ms2 = 0.037077132402;
  OutOfBall = -$FFFFFF;
  GoalHeight = -50;

  txtTrackSubdir = 'Tracks';
  txtElementSubdir = 'Elements';

  txtTrackExt = '.track';
  txtUncompressedTrackExt = '.mig';
  txtCollectionExt = '.tec';
  txtBitmapExt = '.png';

procedure Align(What, ToWhat: TControl; SizeToo: Boolean = False);
procedure AlignSizes(What, ToWhat: TControl);
procedure Resize(What: TControl; AWidth, AHeight: Integer);

{ Original source contains a procedure to set and a function to get
the selected radio button index in a radio group.
I think there's no need for such procedure or function since the
radio group control in the LCL has a property that represents the selected
option. }

{ The original source contained routines for color decomposition.
They are not needed here since they are already contained in the Graphics
unit. }

{ The original source contains functions to convert degrees to radians
and vice versa. These are already declared in the Math unit. }

{ The original source contains a sophisticated rounding function.
I hope that the Round function in the Math unit should suffice, although
it uses "banker's rounding" - it rounds halfs to the closest even number. }

function PlainFileName(FileName: String): String;

function Right(Control: TControl): Integer;
function Bottom(Control: TControl): Integer;

procedure MapBall;
function InBall(X, Y: Integer): Boolean;

function GetMGVersion: String;

implementation

procedure Align(What, ToWhat: TControl; SizeToo: Boolean);
begin
  What.Left:=ToWhat.Left;
  What.Top:=ToWhat.Top;
  if SizeToo then
  begin
    AlignSizes(What, ToWhat);
  end;
end;

procedure AlignSizes(What, ToWhat: TControl);
begin
  What.Width:=ToWhat.Width;
  What.Height:=ToWhat.Height;
end;

procedure Resize(What: TControl; AWidth, AHeight: Integer);
begin
  What.Width:=AWidth;
  What.Height:=AHeight;
end;

function PlainFileName(FileName: String): String;
var
  I: Integer;
  DotPos: Integer;
begin
  DotPos:=0;
  for I:=Length(FileName) downto 1 do
  begin
    if FileName[I] = '.' then
    begin
      DotPos:=I;
      Break;
    end;
  end;
  if DotPos > 0 then
    Result := Copy(FileName, 1, DotPos - 1)
  else // DotPos = 0
    Result := FileName;
end;

function Right(Control: TControl): Integer;
begin
  Result:=Control.Left + Control.Width;
end;

function Bottom(Control: TControl): Integer;
begin
  Result:=Control.Top + Control.Height;
end;

procedure MapBall;
var X, Y: LongInt;
begin
  for X:=0 to FrmBouncingEdgePattern.imgPattern.Picture.Bitmap.Width - 1 do
    for Y:=0 to FrmBouncingEdgePattern.imgPattern.Picture.Bitmap.Height - 1 do
      if InBall(X, Y) then
        BallMap[X, Y]:=sqrt(abs(sqr(BallRadius) -
          (sqr(X - BallRadius) + sqr(Y - BallRadius))))
      else
        BallMap[X, Y]:=OutOfBall;
end;

function InBall(X, Y: Integer): Boolean;
begin
  // If the sum of the squares over both tangents (X and Y)
  // is less than the square over the cotangent (the radius)
  // or equal,
  // then the (X, Y) point is inside the circle.
  // And we add the bounce points for safety reasons.
  Result:=(sqr(X - BallDiameter) + sqr(Y - BallDiameter) <= sqr(BallDiameter))
    or FrmBouncingEdgePattern.OnEdge(X, Y);
end;

function GetMGVersion: String;
begin
  Result := VersionSupport.GetFileVersion;
end;

initialization

finalization

end.

