unit uFrmResults;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  ExtCtrls, Buttons, uMGUtils, sqlite3conn, sqldb;

const
  SMILEY_FRENETIC_SMILE = 0;
  SMILEY_WIDE_SMILE = 1;
  SMILEY_LIGHT_SMILE = 2;
  SMILEY_NEUTRAL_FACE = 3;
  SMILEY_FROWN = 4;

type

  { TFrmResults }

  TFrmResults = class(TForm)
    btClearAllResults: TBitBtn;
    btClearResultsOnThisTrack: TBitBtn;
    btClose: TBitBtn;
    DB: TSQLite3Connection;
    IL: TImageList;
    lvResults: TListView;
    panButtons: TPanel;
    Query: TSQLQuery;
    Tr: TSQLTransaction;
    procedure btClearAllResultsClick(Sender: TObject);
    procedure btClearResultsOnThisTrackClick(Sender: TObject);
    procedure btCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lvResultsCustomDraw(Sender: TCustomListView; const ARect: TRect;
      var DefaultDraw: Boolean);
  private
    { private declarations }
    function HitCountToIconIndex(HitCount: LongInt): Integer;
  public
    { public declarations }
    CurrentTrackName: String;
    procedure WriteResult(TrackName, PlayerName: String; HitCount: LongInt;
      RecordDate: TDateTime);
    procedure ShowResults(TrackName, PlayerName: String; HitCount: LongInt;
      RecordDate: TDateTime);
  end;

var
  FrmResults: TFrmResults;

implementation

uses uFrmMinigolfeeCheck;

{$R *.lfm}

{ TFrmResults }

procedure TFrmResults.FormCreate(Sender: TObject);
begin
  DB.DatabaseName:=OurPath('Results.sqlite');
end;

procedure TFrmResults.btCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmResults.btClearResultsOnThisTrackClick(Sender: TObject);
var
  Resp: TModalResult;
begin
  Resp := MessageDlg('Confirm deletion', 'Do you really want to clear ' +
    'the results achieved on this track?', mtConfirmation, mbYesNo, 0, mbNo);
  if Resp = mrYes then
  begin
    if not Tr.Active then Tr.StartTransaction;
    Query.SQL.Text := Format('delete from results where track=''%s''',
      [CurrentTrackName]);
    Query.ExecSQL;
    Tr.Commit;
    lvResults.Clear;
  end;
end;

procedure TFrmResults.btClearAllResultsClick(Sender: TObject);
var
  Resp: TModalResult;
begin
  Resp := MessageDlg('Confirm deletion', 'Do you really want to clear ' +
    'all the results achieved on all tracks?', mtConfirmation, mbYesNo, 0,
    mbNo);
  if Resp = mrYes then
  begin
    if not Tr.Active then Tr.StartTransaction;
    Query.SQL.Text := 'delete from results';
    Query.ExecSQL;
    Tr.Commit;
    lvResults.Clear;
  end;
end;

procedure TFrmResults.lvResultsCustomDraw(Sender: TCustomListView;
  const ARect: TRect; var DefaultDraw: Boolean);
begin
  Canvas.CopyRect(ARect, FrmMinigolfeeCheck.imgTerrain.Canvas, ARect);
  DefaultDraw:=True;
end;

function TFrmResults.HitCountToIconIndex(HitCount: LongInt): Integer;
begin
  if HitCount = 0 then
    Result := SMILEY_FRENETIC_SMILE // frenetic smile
  else if HitCount = 1 then
    Result := SMILEY_WIDE_SMILE // wide smile
  else if HitCount in [2..6] then
    Result := SMILEY_LIGHT_SMILE // light smile
  else if HitCount = 7 then
    Result := SMILEY_NEUTRAL_FACE // neutral face
  else {if HitCount > 7 then}
    Result := SMILEY_FROWN; // frown
end;

procedure TFrmResults.WriteResult(TrackName, PlayerName: String;
  HitCount: LongInt; RecordDate: TDateTime);
var
  sSQL: String;
  sDate: String;
begin
  // Controller
  sDate := FormatDateTime('yyyy-mm-dd HH:MM:SS', RecordDate);
  // Model -> Data
  sSQL := Format('insert into results (track, player, hit_count, date) ' +
    'values(''%s'', ''%s'', %d, ''%s'')',
    [TrackName, PlayerName, HitCount, sDate]);
  if not Tr.Active then Tr.StartTransaction;
  Query.SQL.Text:=sSQL;
  Query.ExecSQL;
  Tr.Commit;
end;

procedure TFrmResults.ShowResults(TrackName, PlayerName: String;
  HitCount: LongInt; RecordDate: TDateTime);
var
  sSQL: String;
  oNode: TListItem;
  sCaption: String;
  dtDate: TDateTime;
  sDate: String;
  sRecordDate: String;
  sPlayer: String;
  iHitCount: Integer;
  sHitCount: String;
  iHitCountSubnodeIndex: Integer;
  iHitCountIconIndex: Integer;
  oCurrentRecordNode: TListItem;
begin
  CurrentTrackName := TrackName;
  sRecordDate := FormatDateTime('yyyy-mm-dd HH:MM:SS', RecordDate);
  sSQL := Format('select player, hit_count, date from results ' +
    'where track = ''%s'' order by player, hit_count, date', [TrackName]);
  Query.SQL.Text := sSQL;
  Query.Open;

  lvResults.Clear; // The nodes persist over closing the form. Why? :-)

  { A dirty hack to read all the records on the start.
    Without it, if the cursor reaches the 11th record, it skips back to the
    first record. Then, if it reaches the 21st record, it skips back again. }
  Query.Last;
  Query.First;

  oCurrentRecordNode := nil;
  // Fill the list view
  while not Query.EOF do
  begin
    // Model
    sCaption := Format('Results on the “%s” track', [TrackName]);
    sPlayer := Query.FieldByName('player').AsString;
    iHitCount := Query.FieldByName('hit_count').AsInteger;
    sHitCount := IntToStr(iHitCount);
    dtDate := Query.FieldByName('date').AsDateTime;
    sDate := FormatDateTime('yyyy-mm-dd HH:MM:SS', dtDate);
    // View
    Caption := sCaption;
    lvResults.AddItem(sPlayer, nil);
    oNode := lvResults.Items[lvResults.Items.Count - 1];
    iHitCountSubnodeIndex := oNode.SubItems.Add(sHitCount);
    iHitCountIconIndex := HitCountToIconIndex(iHitCount);
    oNode.SubItemImages[iHitCountSubnodeIndex] := iHitCountIconIndex;
    oNode.SubItems.Append(sDate);
    // Assign the current record row node
    if (sPlayer = PlayerName) and (iHitCount = HitCount) and
      (sDate = sRecordDate) then
    begin
      oCurrentRecordNode := oNode;
    end;
    Query.Next;
  end;
  Query.Close;
  // Make this form cover FrmMinigolfeeCheck
  BoundsRect := FrmMinigolfeeCheck.BoundsRect;
  { Copy the terrain image from FrmMinigolfeeCheck into the background
    of the list view }
  lvResults.Canvas.Draw(0, 0, FrmMinigolfeeCheck.imgTerrain.Picture.Graphic);
  // Show this form
  Show;
  // Focus the node with the latest date
  if oCurrentRecordNode <> nil then
  begin
    oCurrentRecordNode.MakeVisible(False);
    lvResults.Selected := oCurrentRecordNode;
    oCurrentRecordNode.Selected := True;
    lvResults.ItemIndex := oCurrentRecordNode.Index;
    if lvResults.CanFocus then
    begin
      oCurrentRecordNode.Focused := True; // doesn't work
      lvResults.ItemFocused := oCurrentRecordNode; // doesn't work
      // What else to try?
      oCurrentRecordNode.Checked := True;
    end;
  end;
end;

end.

