unit uFrmTrackEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs;

type

  { TFrmTrackEditor }

  TFrmTrackEditor = class(TForm)
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
  private
    { private declarations }
  public
    { public declarations }
    procedure Open(FileName: String);
  end;

var
  FrmTrackEditor: TFrmTrackEditor;

implementation

uses uFrmMinigolfeeCheck, uFrmCollection;

{$R *.lfm}

{ TFrmTrackEditor }

procedure TFrmTrackEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  if not FrmMinigolfeeCheck.Visible then
    if not FrmCollection.Visible then
      FrmMinigolfeeCheck.Close;
end;

procedure TFrmTrackEditor.Open(FileName: String);
begin
  Show;
  ShowMessage(FileName);
end;

end.

