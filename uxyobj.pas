unit uXYObj;
{ A coordinate computing object }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, uMGBasicTypes, Math;

type

  { TXYObj }

  TXYObj = class(TObject)
    private
      FAngle: Extended;
      FLength: Extended;
      FX: Extended;
      FY: Extended;
      procedure SetAngle(AValue: Extended);
      procedure SetLength(AValue: Extended);
      procedure SetX(AX: Extended);
      procedure SetY(AY: Extended);
      procedure RefreshXY;
      procedure RefreshLA;
    public
      constructor Create(AX: Extended = 0; AY: Extended = 0); overload;
      constructor Create(XY: TXY); overload;
      procedure SetXY(AX: Extended = 0; AY: Extended = 0); overload;
      procedure SetXY(XY: TXY); overload;
      function Diff(XYO: TXYObj): TXYObj; overload;
      function Diff(XY: TXY): TXYObj; overload;
      function DiffXY(XYO: TXYObj): TXY; overload;
      function DiffXY(XY: TXY): TXY; overload;
      function Sum(XYO: TXYObj): TXYObj; overload;
      function Sum(XY: TXY): TXYObj; overload;
      function SumXY(XYO: TXYObj): TXY; overload;
      function SumXY(XY: TXY): TXY; overload;
    published
      property X: Extended read FX write SetX;
      property Y: Extended read FY write SetY;
      property Angle: Extended read FAngle write SetAngle;
      property Length: Extended read FLength write SetLength;
  end;

function CXYObj(X: Extended = 0; Y: Extended = 0): TXYObj; overload;
function CXYObj(XY: TXY): TXYObj; overload;
function CXY(XYO: TXYObj): TXY; overload;

operator-(A, B: TXYObj): TXYObj;
operator-(A: TXYObj; B: TXY): TXYObj;
operator-(A: TXY; B: TXYObj): TXYObj;
operator+(A, B: TXYObj): TXYObj;
operator+(A: TXYObj; B:TXY): TXYObj;
operator+(A: TXY; B:TXYObj): TXYObj;
operator:=(XY: TXY): TXYObj;

implementation

operator-(A, B: TXYObj): TXYObj;
begin
  Result:=A.Diff(B);
end;

operator-(A: TXYObj; B: TXY): TXYObj;
begin
  Result:=A.Diff(B);
end;

operator-(A: TXY; B: TXYObj): TXYObj;
var
  AXYO: TXYObj;
begin
  AXYO:=A;
  Result:=AXYO.Diff(B);
  AXYO.Free;
end;

operator+(A, B: TXYObj): TXYObj;
begin
  Result:=A.Sum(B);
end;

operator+(A: TXYObj; B: TXY): TXYObj;
begin
  Result:=A.Sum(B);
end;

operator+(A: TXY; B: TXYObj): TXYObj;
var
  AXYO: TXYObj;
begin
  AXYO:=A;
  Result:=AXYO.Sum(B);
  AXYO.Free;
end;

operator:=(XY: TXY): TXYObj;
begin
  Result:=TXYObj.Create(XY);
end;

{ TXYObj }

procedure TXYObj.SetX(AX: Extended);
begin
  if FX=AX then Exit;
  FX:=AX;
  RefreshLA;
end;

procedure TXYObj.SetAngle(AValue: Extended);
begin
  AValue:=AbsAngle(AValue);
  if FAngle=AValue then Exit;
  FAngle:=AValue;
  RefreshXY;
end;

procedure TXYObj.SetLength(AValue: Extended);
begin
  if FLength=AValue then Exit;
  FLength:=AValue;
  RefreshXY;
end;

procedure TXYObj.SetY(AY: Extended);
begin
  if FY=AY then Exit;
  FY:=AY;
  RefreshLA;
end;

procedure TXYObj.RefreshXY;
begin
  if (FAngle = -1) or (FLength = 0) then
  begin
    FX:=0;
    FY:=0;
  end
  else
  begin
    FX:=sin(degtorad(FAngle)) * FLength;
    FY:=-cos(degtorad(FAngle)) * FLength;
  end;
end;

procedure TXYObj.RefreshLA;
begin
  FLength:=sqrt(sqr(FX) + sqr(FY));
  FAngle:=Arcus(CXY(FX, FY));
end;

constructor TXYObj.Create(AX: Extended; AY: Extended); overload;
begin
  inherited Create;
  SetXY(AX, AY);
end;

constructor TXYObj.Create(XY: TXY);
begin
  inherited Create;
  SetXY(XY);
end;

procedure TXYObj.SetXY(AX: Extended; AY: Extended);
begin
  FX:=AX;
  FY:=AY;
  RefreshLA;
end;

procedure TXYObj.SetXY(XY: TXY);
begin
  FX:=XY.X;
  FY:=XY.Y;
  RefreshLA;
end;

function TXYObj.Diff(XYO: TXYObj): TXYObj;
begin
  Result:=TXYObj.Create(FX - XYO.X, FY - XYO.Y);
end;

function TXYObj.Diff(XY: TXY): TXYObj;
begin
  Result:=TXYObj.Create(FX - XY.X, FY - XY.Y);
end;

function TXYObj.DiffXY(XYO: TXYObj): TXY;
begin
  Result.X:=FX - XYO.X;
  Result.Y:=FY - XYO.Y;
end;

function TXYObj.DiffXY(XY: TXY): TXY;
begin
  Result.X:=FX - XY.X;
  Result.Y:=FY - XY.Y;
end;

function TXYObj.Sum(XYO: TXYObj): TXYObj;
begin
  Result:=TXYObj.Create(FX + XYO.X, FY + XYO.Y);
end;

function TXYObj.Sum(XY: TXY): TXYObj;
begin
  Result:=TXYObj.Create(FX + XY.X, FY + XY.Y);
end;

function TXYObj.SumXY(XYO: TXYObj): TXY;
begin
  Result:=CXY(FX + XYO.X, FY + XYO.Y);
end;

function TXYObj.SumXY(XY: TXY): TXY;
begin
  Result:=CXY(FX + XY.X, FY + XY.Y);
end;

function CXYObj(X: Extended; Y: Extended): TXYObj;
begin
  Result:=TXYObj.Create(X, Y);
end;

function CXYObj(XY: TXY): TXYObj;
begin
  Result:=TXYObj.Create(XY);
end;

function CXY(XYO: TXYObj): TXY;
begin
  Result:=CXY(XYO.X, XYO.Y);
end;

end.

