unit uMGBasicTypes;
{ Basic types and classes for MinigolfeeCheck }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math;

type
  TXY = record
    X, Y: Double;
  end;

  TXYLng = TPoint;

  TXYByte = packed record
    X, Y: Byte;
  end;

  TXYZ = record
    X, Y, Z: Extended;
  end;

  TXYLngZVar = record
    X, Y: LongInt;
    Z: Extended;
  end;

  TVectorComponent = (
    vcX,
    vcY,
    vcZ,
    vcXY,
    vcXZ,
    vcYZ,
    vcXYZ
  );

  { Length-Angle Vector }
  TLAV = record
    Length: Extended;
    Angle: Extended;
  end;

  TContactPointRow = array of TXYByte;
  TContactPointPlane = array of TContactPointRow;
  TContactPointMap = array of TContactPointPlane;
  T3DContactPointArray = array of TXYLngZVar;


type
  TSelectionKind = (
    skRelief,
    skTexture,
    skElement
  );

  TViewKind = (
    vkRelief,
    vkTexture,
    vkShadedTexture
  );

  TReliefOperation = (
    ropInsert,
    ropAdd,
    ropSubtract,
    ropAverage,
    ropUnion,
    ropIntersection
  );

  TReliefColumn = array of Single;
  TRelief = array of TReliefColumn;

operator-(A, B: TXY): TXY;
operator+(A, B: TXY): TXY;
function CXY(X: Extended = 0; Y: Extended = 0): TXY;
function CXY(XYLng: TXYLng): TXY;
function CXY(XYByte: TXYByte): TXY;
function XYLngToXY(XYLng: TXYLng): TXY;
function CXYByte(X: Byte; Y: Byte): TXYByte;
function CXYLng(X: LongInt; Y: LongInt): TXYLng;
function CXYZ(X: LongInt; Y: LongInt; Z: Extended): TXYLngZVar;

function XYDiff(XY1, XY2: TXY): TXY;
function XYSum(XY1, XY2: TXY): TXY;
function XYShift(XY: TXY; Shift: Extended): TXY;
function XYNeg(XY: TXY): TXY;

function tg(Degrees: Extended): Extended;
function cotg(Degrees: Extended): Extended;
function arctg(Tangens: Extended): Extended;
function arccotg(Cotangens: Extended): Extended;

function IsZeroVector(Vector: TXY): Boolean;
function Arcus(Vector: TXY): Extended;
function ArcusAbAxe(Vector: TXY; VectorAxalis: TXY): Extended;
function AbsAngle(Angle: Extended): Extended;
function XYToLAV(Vector: TXY): TLAV;
function LavToXY(Vector: TLAV): TXY;


procedure ResizeRelief(var Relief: TRelief; Width, Height: Integer);

implementation

operator+(A, B: TXY): TXY;
begin
  Result:=XYSum(A, B);
end;

function CXY(X: Extended; Y: Extended): TXY;
begin
  Result.X:=X;
  Result.Y:=Y;
end;

function CXY(XYLng: TXYLng): TXY;
begin
  Result.X:=XYLng.X;
  Result.Y:=XYLng.Y;
end;

function CXY(XYByte: TXYByte): TXY;
begin
  Result.X:=XYByte.X;
  Result.Y:=XYByte.Y;
end;

function XYLngToXY(XYLng: TXYLng): TXY;
begin
  Result := CXY(XYLng);
end;

function CXYByte(X: Byte; Y: Byte): TXYByte;
begin
  Result.X:=X;
  Result.Y:=Y;
end;

function CXYLng(X: LongInt; Y: LongInt): TXYLng;
begin
  Result.X:=X;
  Result.Y:=Y;
end;

function CXYZ(X: LongInt; Y: LongInt; Z: Extended): TXYLngZVar;
begin
  Result.X:=X;
  Result.Y:=Y;
  Result.Z:=Z;
end;

function XYDiff(XY1, XY2: TXY): TXY;
begin
  Result.X:=XY1.X - XY2.X;
  Result.Y:=XY1.Y - XY2.Y;
end;

function XYSum(XY1, XY2: TXY): TXY;
begin
  Result.X:=XY1.X + XY2.X;
  Result.Y:=XY1.Y + XY2.Y;
end;

function XYShift(XY: TXY; Shift: Extended): TXY;
begin
  Result.X:=XY.X + Shift;
  Result.Y:=XY.Y + Shift;
end;

function XYNeg(XY: TXY): TXY;
begin
  Result.X:=-XY.X;
  Result.Y:=-XY.Y;
end;

operator-(A, B: TXY): TXY;
begin
  Result:=XYDiff(A, B);
end;

function tg(Degrees: Extended): Extended;
begin
  Result:=tan(degtorad(Degrees));
end;

function cotg(Degrees: Extended): Extended;
begin
  Result:=cotan(degtorad(Degrees));
end;

function arctg(Tangens: Extended): Extended;
begin
  Result:=radtodeg(arctan(Tangens));
end;

function arccotg(Cotangens: Extended): Extended;
begin
  if Cotangens = 0 then
    Result := 90
  else
    Result:=radtodeg(arctan(1 / Cotangens));
end;

function IsZeroVector(Vector: TXY): Boolean;
begin
  Result:=(Vector.X = 0) and (Vector.Y = 0);
end;

function Arcus(Vector: TXY): Extended;
var
  Tangens: Extended;
  RawAngle: Extended;
begin
  if IsZeroVector(Vector) then
    Result:=-1
  else
  begin
    // If it's on the horizontal axis
    if Vector.Y = 0 then
    begin
      if Vector.X = 0 then
        raise Exception.Create('Can''t tell the angle - zero vector given.')
      else if Vector.X > 0 then
        RawAngle:=90
      else if Vector.X < 0 then
        RawAngle:=270;
    end
    else // If it isn't on the horizontal axis
    begin
      Tangens:=Vector.X / -Vector.Y;
      RawAngle:=arctg(Tangens);
    end;
    if -Vector.Y >= 0 then
      Result:=RawAngle
    else
      Result:=RawAngle + 180;
  end;
end;

function ArcusAbAxe(Vector: TXY; VectorAxalis: TXY): Extended;
var
  NonnegativeResult: Extended;
begin
  NonnegativeResult:=2 * 360 + Arcus(Vector) - Arcus(VectorAxalis);
  Result:=AbsAngle(NonnegativeResult);
end;

function AbsAngle(Angle: Extended): Extended;
begin
  Result:=Angle - (Trunc(Angle) div 360) * 360;
end;

function XYToLAV(Vector: TXY): TLAV;
begin
  Result.Length:=sqrt(sqr(Vector.X) + sqr(Vector.Y));
  Result.Angle:=Arcus(Vector);
end;

function LavToXY(Vector: TLAV): TXY;
var
  Angle: Extended;
begin
  Angle:=AbsAngle(Vector.Angle);
  if (Angle = -1) or (Vector.Length = 0) then
    Result := CXY()
  else
  begin
    Result.X:=sin(degtorad(Angle)) * Vector.Length;
    Result.Y:=-cos(degtorad(Angle)) * Vector.Length;
  end;
end;

procedure ResizeRelief(var Relief: TRelief; Width, Height: Integer);
begin
  SetLength(Relief, Width, Height);
end;

end.

