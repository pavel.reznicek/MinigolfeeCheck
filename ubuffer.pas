unit uBuffer;
{ Contains a clipboard object to hold and transfer chunks of terrain
as relief and texture }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, uMGBasicTypes;

type TBufferChangeEvent = procedure(TextureOnly: Boolean) of object;

type

{ TBuffer }
{ Contains shared texture and shared relief - serves as a clipboard. }

 TBuffer = class(TObject)
  private
    FRelief: TRelief;
    FTexture: TBitmap;
    FOnChange: TBufferChangeEvent;
    FWidth: Integer;
    FHeight: Integer;
    procedure SetRelief(ARelief: TRelief);
    function GetRelief: TRelief;
    procedure SetTexture(ATexture: TBitmap);
    function GetTexture: TBitmap;
    procedure SetWidth(AWidth: Integer);
    function GetWidth: Integer;
    procedure SetHeight(AHeight: Integer);
    function GetHeight: Integer;
    procedure SetReliefPoint(X, Y: Integer; AValue: Single);
    function GetReliefPoint(X, Y: Integer): Single;
  public
    procedure Resize(AWidth, AHeight: Integer);
    procedure ApplyChanges(TextureOnly: Boolean = False);
    property ReliefPoint[X, Y: Integer]: Single read GetReliefPoint
      write SetReliefPoint;
  published
    property Width: Integer read GetWidth write SetWidth;
    property Height: Integer read GetHeight write SetHeight;
    property OnChange: TBufferChangeEvent read FOnChange write FOnChange;
end;

implementation

{ TBuffer }

procedure TBuffer.SetReliefPoint(X, Y: Integer; AValue: Single);
begin
  if  (X >= 0) and (X < FWidth)
  and (Y >= 0) and (Y < FHeight) then
    FRelief[X, Y] := AValue;
  ApplyChanges();
end;

procedure TBuffer.SetRelief(ARelief: TRelief);
begin
  FRelief:=ARelief;
  ApplyChanges();
end;

function TBuffer.GetRelief: TRelief;
begin
  Result:=FRelief;
end;

procedure TBuffer.SetTexture(ATexture: TBitmap);
begin
  FTexture:=ATexture;
  ApplyChanges(True);
end;

function TBuffer.GetTexture: TBitmap;
begin
  Result:=FTexture;
end;

procedure TBuffer.SetWidth(AWidth: Integer);
begin
  FWidth:=AWidth;
  ResizeRelief(FRelief, FWidth, FHeight);
  ApplyChanges;
end;

function TBuffer.GetWidth: Integer;
begin
  Result:=FWidth;
end;

procedure TBuffer.SetHeight(AHeight: Integer);
begin
  FHeight:=AHeight;
  ResizeRelief(FRelief, FWidth, FHeight);
  ApplyChanges;
end;

function TBuffer.GetHeight: Integer;
begin
  Result:=FHeight;
end;

procedure TBuffer.Resize(AWidth, AHeight: Integer);
begin
  FWidth:=AWidth;
  FHeight:=AHeight;
  ResizeRelief(FRelief, FWidth, FHeight);
  ApplyChanges;
end;

procedure TBuffer.ApplyChanges(TextureOnly: Boolean = False);
begin
  { The original procedure used the form objects directly to display the
  buffer contents. We should use an OnChange event handler instead. }
  if FOnChange <> nil then FOnChange(TextureOnly);
end;

function TBuffer.GetReliefPoint(X, Y: Integer): Single;
begin
  if  (X >= 0) and (X < FWidth)
  and (Y >= 0) and (Y < FHeight) then
    Result:=FRelief[X, Y];
end;

end.

