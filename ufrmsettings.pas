unit uFrmSettings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, Buttons, StdCtrls, uMGSettings, uDM;

type

  { TFrmSettings }

  TFrmSettings = class(TForm)
    bbOK: TBitBtn;
    bbCancel: TBitBtn;
    bbApply: TBitBtn;
    cbAnchorBall: TCheckBox;
    cbSound: TCheckBox;
    pcSettings: TPageControl;
    panButtons: TPanel;
    tsGame: TTabSheet;
    procedure bbApplyClick(Sender: TObject);
    procedure bbCancelClick(Sender: TObject);
    procedure bbOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    procedure ReadValues;
    procedure WriteValues;
  end;

var
  FrmSettings: TFrmSettings;

implementation

{$R *.lfm}

{ TFrmSettings }

procedure TFrmSettings.FormCreate(Sender: TObject);
begin
  ReadValues;
end;

procedure TFrmSettings.ReadValues;
begin
  // Game settings
  cbSound.Checked:=DM.Settings.SoundEnabled;
  cbAnchorBall.Checked:=DM.Settings.AnchorBall;
end;

procedure TFrmSettings.WriteValues;
begin
  // Game settings
  DM.Settings.SoundEnabled:=cbSound.Checked;
  DM.Settings.AnchorBall:=cbAnchorBall.Checked;
end;

procedure TFrmSettings.bbOKClick(Sender: TObject);
begin
  WriteValues;
  Close;
end;

procedure TFrmSettings.bbApplyClick(Sender: TObject);
begin
  WriteValues;
end;

procedure TFrmSettings.bbCancelClick(Sender: TObject);
begin
  Close;
end;

end.

