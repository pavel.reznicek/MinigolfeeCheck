unit uMGUtils;
{ Basic utility functions for MinigolfeeCheck }

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, LazFileUtils;

function OurPath(RelativePath: String): String;
function SoundPath(SoundName: String): String;

implementation

function OurPath(RelativePath: String): String;
var
  ExeName: String;
  RealPath: String;
begin
  ExeName:=Application.ExeName;
  RealPath:=ReadAllLinks(ExeName, False);
  Result:=ExtractFilePath(RealPath) + RelativePath;
end;

function SoundPath(SoundName: String): String;
const
  txtSoundFolder = 'Sounds';
begin
  Result := OurPath(txtSoundFolder + PathDelim + SoundName + '.wav');
end;


end.

