#2017-10-07 Effects
  - This is already a very old idea. Various physical and “supernatural”
    effects could be added to manipulate the ball like in the Hubert
    Achthaler’s Golph games. Various bouncers, tunnels, tobogans, and teleports.

#2017-10-07 Multi-Window Tracks
  - This idea came to me through a dream. A single track could be distributed
    in several windows. The windows could be connected via teleports.
    There should be an initial window layout defined for each multi-window
    track.
