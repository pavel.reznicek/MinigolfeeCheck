unit uFrmCollection;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs;

type
  TFrmCollection = class(TForm)
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FrmCollection: TFrmCollection;

implementation

{$R *.lfm}

end.

