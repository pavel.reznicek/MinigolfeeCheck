# MinigolfeeCheck
A miniature golf simulation game

This game was originally written in Visual Basic back in the year 2000 by
Pavel Řezníček A. K. A. Cigydd (pavel.reznicek@evangnet.cz).

What you have here is a rewrite of the original code into FreePascal with the
Lazarus IDE and LCL (Lazarus Component Library).

It uses a simple bird’s eye perspective and a pseudo-3D environment for the
gameplay.

It provides a purpose-centered track editor with an ability to create so called 
terrain element collections, a kind of track templates.

It’s distributed under the terms of the GNU/GPL (GNU General Public
License), version 3. You should have received a copy of it in the LICENSE file
with this software.
