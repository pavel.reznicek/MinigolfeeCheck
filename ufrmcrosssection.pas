unit uFrmCrossSection;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  uMGSettings, uDM, uMGBasicTypes;

type

  { TFrmCrossSection }

  TFrmCrossSection = class(TForm)
    imgCrossSection: TImage;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
    FScaleLeft: Double;
    FScaleTop: Double;
    FScaleWidth: Double;
    FScaleHeight: Double;
    procedure SetScaleLeft(AValue: Double);
    procedure SetScaleTop(AValue: Double);
    procedure SetScaleWidth(AValue: Double);
    procedure SetScaleHeight(AValue: Double);
  public
    { public declarations }
    procedure ScaleLine(P1, P2: TXY);
    procedure ScalePoint(P: TXY);
    procedure ScaleCircle(Center: TXY; Radius: Double);
    function ScalePointToClient(Point: TXY): TPoint;
    function ScaleSizeToClient(Size: TXY): TPoint;
    procedure Clear;
    property ScaleLeft: Double read FScaleLeft write SetScaleLeft;
    property ScaleTop: Double read FScaleTop write SetScaleTop;
    property ScaleWidth: Double read FScaleWidth write SetScaleWidth;
    property ScaleHeight: Double read FScaleHeight write SetScaleHeight;
  end;

var
  FrmCrossSection: TFrmCrossSection;

implementation

{$R *.lfm}

{ TFrmCrossSection }

procedure TFrmCrossSection.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  if DM.Settings.ShowForces then
    DM.Settings.ShowForces:=False;
end;

procedure TFrmCrossSection.FormCreate(Sender: TObject);
begin
  ClientWidth := ClientHeight;
  ScaleLeft := -50;
  ScaleWidth := 100;
  ScaleTop := 99;
  ScaleHeight := -100;
end;

procedure TFrmCrossSection.FormShow(Sender: TObject);
begin

end;

procedure TFrmCrossSection.SetScaleHeight(AValue: Double);
begin
  if FScaleHeight=AValue then Exit;
  FScaleHeight:=AValue;
end;

procedure TFrmCrossSection.ScaleLine(P1, P2: TXY);
var
  ClientP1, ClientP2: TPoint;
begin
  ClientP1 := ScalePointToClient(P1);
  ClientP2 := ScalePointToClient(P2);
  imgCrossSection.Canvas.Line(ClientP1, ClientP2);
end;

procedure TFrmCrossSection.ScalePoint(P: TXY);
begin
  ScaleLine(P, P);
end;

procedure TFrmCrossSection.ScaleCircle(Center: TXY; Radius: Double);
var
  ClientCenter: TPoint;
  ClientRadius: TPoint;
begin
  ClientCenter := ScalePointToClient(Center);
  ClientRadius := ScaleSizeToClient(CXY(Radius, Radius));
  imgCrossSection.Canvas.Ellipse(
    round(ClientCenter.X - ClientRadius.X),
    round(ClientCenter.Y - ClientRadius.Y),
    round(ClientCenter.X + ClientRadius.X),
    round(ClientCenter.Y + ClientRadius.Y)
  );
end;

function TFrmCrossSection.ScaleSizeToClient(Size: TXY): TPoint;
begin
  Result.X := round(imgCrossSection.ClientWidth / ScaleWidth * Size.X);
  Result.Y := round(imgCrossSection.ClientHeight / ScaleHeight * Size.Y);
end;

procedure TFrmCrossSection.Clear;
begin
  imgCrossSection.Canvas.Clear;
end;

function TFrmCrossSection.ScalePointToClient(Point: TXY): TPoint;
begin
  Result.X := round(imgCrossSection.ClientWidth / ScaleWidth *
    (Point.X - ScaleLeft));
  Result.Y := round(imgCrossSection.ClientHeight / ScaleHeight *
    (Point.Y - ScaleTop));
end;

procedure TFrmCrossSection.SetScaleLeft(AValue: Double);
begin
  if FScaleLeft=AValue then Exit;
  FScaleLeft:=AValue;
end;

procedure TFrmCrossSection.SetScaleTop(AValue: Double);
begin
  if FScaleTop=AValue then Exit;
  FScaleTop:=AValue;
end;

procedure TFrmCrossSection.SetScaleWidth(AValue: Double);
begin
  if FScaleWidth=AValue then Exit;
  FScaleWidth:=AValue;
end;

end.

