unit uFrmBouncingEdgePattern;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  uMGBasicTypes;

type

  { TFrmBouncingEdgePattern }

  TFrmBouncingEdgePattern = class(TForm)
    imgPattern: TImage;
    procedure FormCreate(Sender: TObject);
    procedure imgPatternClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    function OnEdge(X, Y: Integer): Boolean;
    function OnEdge(XY: TXYLng): Boolean;
    function OnEdge(XYZ: TXYLngZVar): Boolean;
  end;

var
  FrmBouncingEdgePattern: TFrmBouncingEdgePattern;

implementation

{$R *.lfm}

uses uMGSharedItems;

{ TFrmBouncingEdgePattern }

procedure TFrmBouncingEdgePattern.imgPatternClick(Sender: TObject);
begin

end;

procedure TFrmBouncingEdgePattern.FormCreate(Sender: TObject);
begin
  MapBall;
end;

function TFrmBouncingEdgePattern.OnEdge(X, Y: Integer): Boolean;
begin
  Result:=(imgPattern.Picture.Bitmap.Canvas.Pixels[X, Y] = clBlack);
end;

function TFrmBouncingEdgePattern.OnEdge(XY: TXYLng): Boolean;
begin
  Result:=OnEdge(XY.X, XY.Y);
end;

function TFrmBouncingEdgePattern.OnEdge(XYZ: TXYLngZVar): Boolean;
begin
  Result:=OnEdge(XYZ.X, XYZ.Y);
end;

end.

