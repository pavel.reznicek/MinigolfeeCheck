unit uMGSettings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, XMLConf, Variants, Forms, Graphics, uMGBasicTypes;

var
  //Config: TXMLConfig;
  ConfigPath: String;

type

  TBuildPermissionChangeEvent =
    procedure(Kind: TSelectionKind; NewValue: Boolean) of object;
  TBuildPermissionChangeEventArray = array of TBuildPermissionChangeEvent;
  TChangeEvent = procedure() of object;
  TChangeEventArray = array of TChangeEvent;

  { TMGSettings }

  TMGSettings = class(TObject)
    private
      FOnAimWithSlingChange: TChangeEventArray;
      FOnAnchorBallChange: TChangeEventArray;
      FOnArrowBodyColorChange: TChangeEventArray;
      FOnArrowHeadColorChange: TChangeEventArray;
      FOnArrowWidthChange: TChangeEventArray;
      FOnBuildPermissionChange: TBuildPermissionChangeEventArray;
      FOnBuildPermissionChangeEnabled: TChangeEventArray;
      FBuildPermissionChangeEnabled: Boolean;
      FOnShowForces: TChangeEventArray;
      FOnSoundEnable: TChangeEventArray;
      function GetAnchorBall: Boolean;
      function GetCheatingEnabled: Boolean;
      function GetLastPlayerName: String;
      function GetShowForces: Boolean;
      function GetSoundEnabled: Boolean;
      function GetTimer: Boolean;
      function GetTraceMode: Boolean;
      function GetBallVelocity: LongInt;
      procedure RegisterChangeEvent(var EventArray: TChangeEventArray;
        Handler: TChangeEvent);
      procedure FireChangeEventArray(EventArray: TChangeEventArray);
      procedure FireBuildPermissionChangeEventArray(Kind: TSelectionKind;
        NewValue: Boolean);
      function GetArrowBodyColor: TColor;
      function GetArrowHeadColor: TColor;
      function GetArrowWidth: Integer;
      function GetBuildPermission(Kind: TSelectionKind): Boolean;
      procedure SetAimWithSling(AValue: Boolean);
      function GetAimWithSling(): Boolean;
      procedure SetAnchorBall(AValue: Boolean);
      procedure SetArrowBodyColor(AValue: TColor);
      procedure SetArrowHeadColor(AValue: TColor);
      procedure SetArrowWidth(AValue: Integer);
      procedure SetBuildPermission(Kind: TSelectionKind; Value: Boolean);
      function GetBuildPermissionChangeEnabled: Boolean;
      procedure SetBuildPermissionChangeEnabled(Value: Boolean);
      procedure SetCheatingEnabled(AValue: Boolean);
      procedure SetLastPlayerName(AValue: String);
      procedure SetShowForces(AValue: Boolean);
      procedure SetSoundEnabled(AValue: Boolean);
      procedure SetTimer(AValue: Boolean);
      procedure SetTraceMode(AValue: Boolean);
      procedure SetBallVelocity(AValue: LongInt);
    public
      Config: TXMLConfig;
      constructor Create;
      destructor Destroy; override;
      // An attempt to emulate VisualBasic's registry treatment functions.
      function GetSetting(Section, Key: WideString; Default: Variant): Variant;
      procedure SaveSetting(Section, Key: WideString; Setting: Variant);
      // Events registation
      procedure RegisterAimWithSlingChangeEvent(Handler: TChangeEvent);
      procedure RegisterArrowBodyColorChangeEvent(Handler: TChangeEvent);
      procedure RegisterArrowHeadColorChangeEvent(Handler: TChangeEvent);
      procedure RegisterArrowWidthChangeEvent(Handler: TChangeEvent);
      procedure RegisterBuildPermissionChangeEvent(Handler:
        TBuildPermissionChangeEvent);
      procedure RegisterShowForcesChangeEvent(Handler: TChangeEvent);
      procedure RegisterSoundEnabledChangeEvent(Handler: TChangeEvent);
      property BuildPermission[Kind: TSelectionKind]: Boolean
        read GetBuildPermission write SetBuildPermission;
    published
      property BuildPermissionChangeEnabled: Boolean
        read GetBuildPermissionChangeEnabled
        write SetBuildPermissionChangeEnabled;
      property AimWithSling: Boolean read GetAimWithSling write SetAimWithSling;
      property ArrowWidth: Integer read GetArrowWidth write SetArrowWidth;
      property ArrowBodyColor: TColor read GetArrowBodyColor
        write SetArrowBodyColor;
      property ArrowHeadColor: TColor read GetArrowHeadColor
        write SetArrowHeadColor;
      property ShowForces: Boolean read GetShowForces write SetShowForces;
      property AnchorBall: Boolean read GetAnchorBall write SetAnchorBall;
      property Timer: Boolean read GetTimer write SetTimer;
      property SoundEnabled: Boolean read GetSoundEnabled write SetSoundEnabled;
      property TraceMode: Boolean read GetTraceMode write SetTraceMode;
      property BallVelocity: LongInt read GetBallVelocity
        write SetBallVelocity;
      property LastPlayerName: String read GetLastPlayerName
        write SetLastPlayerName;
      property CheatingEnabled: Boolean read GetCheatingEnabled
        write SetCheatingEnabled;
  end;

{var
  Settings: TMGSettings;}

const
  txtCommonSettings = 'CommonSettings';
  txtBuildPermissionFor = 'BuildPermissionFor';
  txtBuildPermissions: array[skRelief..skTexture] of WideString = (
    txtBuildPermissionFor + 'Relief',
    txtBuildPermissionFor + 'Texture'
  );
  txtAimWithSling = 'AimWithSling';
  txtArrowWidth = 'ArrowWidth';
  txtArrowBodyColor = 'ArrowBodyColor';
  txtArrowHeadColor = 'ArrowHeadColor';
  txtShowForces = 'ShowForces';
  txtAnchorBall = 'AnchorBall';
  txtTimer = 'Timer';
  txtSoundEnabled = 'SoundEnabled';
  txtTraceMode = 'TraceMode';
  txtVelocityOfView = 'VelocityOfView';
  txtLastPlayerName = 'LastPlayerName';
  txtCheatingEnabled = 'CheatingEnabled';

implementation


{ TMGSettings }

function TMGSettings.GetBuildPermission(Kind: TSelectionKind): Boolean;
begin
  if Kind = skElement then
    raise Exception.Create('Build permission for selection kind "whole ' +
      'element" doesn''t exist.');
  Result := GetSetting(txtCommonSettings, txtBuildPermissions[Kind], True);
end;

function TMGSettings.GetArrowBodyColor: TColor;
begin
  Result:=GetSetting(txtCommonSettings, txtArrowBodyColor, clPurple);
end;

function TMGSettings.GetArrowHeadColor: TColor;
begin
  Result:=GetSetting(txtCommonSettings, txtArrowHeadColor, clPurple);
end;

function TMGSettings.GetArrowWidth: Integer;
begin
  Result:=GetSetting(txtCommonSettings, txtArrowWidth, 1);
end;

procedure TMGSettings.SetAimWithSling(AValue: Boolean);
var
  Saved: Boolean;
begin
  Saved:=GetAimWithSling();
  if Saved=AValue then Exit;
  SaveSetting(txtCommonSettings, txtAimWithSling, AValue);
  FireChangeEventArray(FOnAimWithSlingChange);
end;

function TMGSettings.GetAimWithSling: Boolean;
begin
  Result:=GetSetting(txtCommonSettings, txtAimWithSling, True);
end;

procedure TMGSettings.SetAnchorBall(AValue: Boolean);
var
  Saved: Boolean;
begin
  Saved:=GetAnchorBall;
  if Saved = AValue then Exit;
  SaveSetting(txtCommonSettings, txtAnchorBall, AValue);
  FireChangeEventArray(FOnAnchorBallChange);
end;

procedure TMGSettings.SetArrowBodyColor(AValue: TColor);
var
  Saved: TColor;
begin
  Saved:=GetArrowBodyColor;
  if Saved = AValue then Exit;
  SaveSetting(txtCommonSettings, txtArrowBodyColor, AValue);
  FireChangeEventArray(FOnArrowBodyColorChange);
end;

procedure TMGSettings.SetArrowHeadColor(AValue: TColor);
var
  Saved: TColor;
begin
  Saved:= GetArrowHeadColor;
  if Saved = AValue then Exit;
  SaveSetting(txtCommonSettings, txtArrowHeadColor, AValue);
  FireChangeEventArray(FOnArrowHeadColorChange);
end;

procedure TMGSettings.SetArrowWidth(AValue: Integer);
var
  Saved: Integer;
begin
  Saved:=GetArrowWidth;
  if Saved = AValue then Exit;
  SaveSetting(txtCommonSettings, txtArrowWidth, AValue);
  FireChangeEventArray(FOnArrowWidthChange);
end;

procedure TMGSettings.SetBuildPermission(Kind: TSelectionKind; Value: Boolean);
begin
  if Kind = skElement then
    raise Exception.Create('Build permission for selection kind "whole ' +
      'element" doesn''t exist.');
  SaveSetting(txtCommonSettings, txtBuildPermissions[Kind], Value);
  FireBuildPermissionChangeEventArray(Kind, Value);
end;

function TMGSettings.GetBuildPermissionChangeEnabled: Boolean;
begin
  Result:=FBuildPermissionChangeEnabled;
end;

procedure TMGSettings.SetBuildPermissionChangeEnabled(Value: Boolean);
var
  OldValue: Boolean;
begin
  OldValue:=GetBuildPermissionChangeEnabled;
  if Value <> OldValue then
  begin
    FBuildPermissionChangeEnabled:=Value;
    FireChangeEventArray(FOnBuildPermissionChangeEnabled);
  end;
end;

procedure TMGSettings.SetCheatingEnabled(AValue: Boolean);
var
  OldValue: Boolean;
begin
  OldValue:=GetCheatingEnabled;
  if AValue <> OldValue then
  begin
    SaveSetting(txtCommonSettings, txtCheatingEnabled, AValue);
  end;
end;

procedure TMGSettings.SetLastPlayerName(AValue: String);
var
  OldValue: String;
begin
  OldValue:=GetLastPlayerName;
  if AValue <> OldValue then
  begin
    SaveSetting(txtCommonSettings, txtLastPlayerName, AValue);
  end;
end;

procedure TMGSettings.SetShowForces(AValue: Boolean);
var
  OldValue: Boolean;
begin
  OldValue:=GetShowForces;
  if AValue <> OldValue then
  begin
    SaveSetting(txtCommonSettings, txtShowForces, AValue);
    FireChangeEventArray(FOnShowForces);
  end;
end;

procedure TMGSettings.SetSoundEnabled(AValue: Boolean);
var
  OldValue: Boolean;
begin
  OldValue:=GetSoundEnabled;
  if AValue <> OldValue then
  begin
    SaveSetting(txtCommonSettings, txtSoundEnabled, AValue);
    FireChangeEventArray(FOnSoundEnable);
  end;
end;

procedure TMGSettings.SetTimer(AValue: Boolean);
begin
  SaveSetting(txtCommonSettings, txtTimer, AValue);
end;

procedure TMGSettings.SetTraceMode(AValue: Boolean);
begin
  SaveSetting(txtCommonSettings, txtTraceMode, AValue);
end;

procedure TMGSettings.SetBallVelocity(AValue: LongInt);
begin
  SaveSetting(txtCommonSettings, txtVelocityOfView, AValue);
end;

constructor TMGSettings.Create;
begin
  inherited Create;
  Config:=TXMLConfig.Create(nil);
  ConfigPath:=ExtractFilePath(Application.ExeName) + 'MinigolfeeCheck.xml';
  Config.FileName:=ConfigPath;
end;

destructor TMGSettings.Destroy;
begin
  Config.Free;
end;

function TMGSettings.GetSetting(Section, Key: WideString; Default: Variant): Variant;
begin
  Config.OpenKey(Section);
  case VarType(Default) of
  vtAnsiString, vtUnicodeString, vtString, vtWideString:
    Result := Config.GetValue(Key, WideString(Default));
  vtInteger:
    Result := Config.GetValue(Key, Integer(Default));
  vtBoolean:
    Result := Config.GetValue(Key, Boolean(Default));
  else
    Result := Config.GetValue(Key, WideString(Default));
  end;
  Config.CloseKey;
end;

procedure TMGSettings.SaveSetting(Section, Key: WideString; Setting: Variant);
begin
  Config.OpenKey(Section);
  case VarType(Setting) of
  vtAnsiString, vtUnicodeString, vtString, vtWideString:
    Config.SetValue(Key, WideString(Setting));
  vtInteger:
    Config.SetValue(Key, Integer(Setting));
  vtBoolean:
    Config.SetValue(Key, Boolean(Setting));
  else
    Config.SetValue(Key, WideString(Setting));
  end;
  Config.CloseKey;
  Config.Flush;
end;

function TMGSettings.GetShowForces: Boolean;
begin
  Result:=GetSetting(txtCommonSettings, txtShowForces, False);
end;

function TMGSettings.GetSoundEnabled: Boolean;
begin
  Result:=GetSetting(txtCommonSettings, txtSoundEnabled, True);
end;

function TMGSettings.GetTimer: Boolean;
begin
  Result:=GetSetting(txtCommonSettings, txtTimer, False);
end;

function TMGSettings.GetTraceMode: Boolean;
begin
  Result := GetSetting(txtCommonSettings, txtTraceMode, False);
end;

function TMGSettings.GetBallVelocity: LongInt;
begin
  Result := GetSetting(txtCommonSettings, txtVelocityOfView, 1);
end;

function TMGSettings.GetAnchorBall: Boolean;
begin
  Result := GetSetting(txtCommonSettings, txtAnchorBall, True);
end;

function TMGSettings.GetCheatingEnabled: Boolean;
begin
  Result := GetSetting(txtCommonSettings, txtCheatingEnabled, False);
end;

function TMGSettings.GetLastPlayerName: String;
begin
  Result := GetSetting(txtCommonSettings, txtLastPlayerName,
    SysUtils.GetEnvironmentVariable('USER'));
end;

procedure TMGSettings.RegisterChangeEvent(var EventArray:
  TChangeEventArray; Handler: TChangeEvent);
begin
  SetLength(EventArray, Length(EventArray) + 1);
  EventArray[High(EventArray)]:=Handler;
end;

procedure TMGSettings.FireChangeEventArray(EventArray: TChangeEventArray);
var I: Integer;
begin
  for I:=Low(EventArray) to High(EventArray) do
    if Assigned(EventArray[I]) then
      EventArray[I]();
end;

procedure TMGSettings.FireBuildPermissionChangeEventArray(Kind: TSelectionKind;
  NewValue: Boolean);
var
  I: Integer;
begin
  for I:=Low(FOnBuildPermissionChange) to High(FOnBuildPermissionChange) do
    if Assigned(FOnBuildPermissionChange[I]) then
      FOnBuildPermissionChange[I](Kind, NewValue);
end;

procedure TMGSettings.RegisterAimWithSlingChangeEvent(Handler: TChangeEvent);
begin
  RegisterChangeEvent(FOnAimWithSlingChange, Handler);
end;

procedure TMGSettings.RegisterArrowBodyColorChangeEvent(Handler:
  TChangeEvent);
begin
  RegisterChangeEvent(FOnArrowBodyColorChange, Handler);
end;

procedure TMGSettings.RegisterArrowHeadColorChangeEvent(Handler: TChangeEvent);
begin
  RegisterChangeEvent(FOnArrowHeadColorChange, Handler);
end;

procedure TMGSettings.RegisterArrowWidthChangeEvent(Handler: TChangeEvent);
begin
  RegisterChangeEvent(FOnArrowWidthChange, Handler);
end;

procedure TMGSettings.RegisterBuildPermissionChangeEvent(
  Handler: TBuildPermissionChangeEvent);
begin
  SetLength(FOnBuildPermissionChange, Length(FOnBuildPermissionChange) + 1);
  FOnBuildPermissionChange[High(FOnBuildPermissionChange)]:=Handler;
end;

procedure TMGSettings.RegisterShowForcesChangeEvent(Handler: TChangeEvent);
begin
  RegisterChangeEvent(FOnShowForces, Handler);
end;

procedure TMGSettings.RegisterSoundEnabledChangeEvent(Handler: TChangeEvent);
begin
  RegisterChangeEvent(FOnSoundEnable, Handler);
end;

initialization

finalization

end.

