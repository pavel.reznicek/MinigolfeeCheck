program MinigolfeeCheck;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, uFrmMinigolfeeCheck, uMGSharedItems, uBuffer, uMGBasicTypes, uXYObj,
  uMGSettings, uMGUtils, uFrmBouncingEdgePattern, uFrmForces, uFrmCrossSection,
  uFrmSettings, uDM, uFrmTrackEditor, uFrmCollection, uFrmElementEditor,
  uFrmInfo, uFrmResults, VersionSupport;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(TFrmMinigolfeeCheck, FrmMinigolfeeCheck);
  Application.CreateForm(TFrmBouncingEdgePattern, FrmBouncingEdgePattern);
  Application.CreateForm(TFrmForces, FrmForces);
  Application.CreateForm(TFrmCrossSection, FrmCrossSection);
  Application.CreateForm(TFrmSettings, FrmSettings);
  Application.CreateForm(TFrmTrackEditor, FrmTrackEditor);
  Application.CreateForm(TFrmCollection, FrmCollection);
  Application.CreateForm(TFrmElementEditor, FrmElementEditor);
  Application.CreateForm(TFrmInfo, FrmInfo);
  Application.CreateForm(TFrmResults, FrmResults);
  Application.Run;
end.

